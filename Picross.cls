VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPicross"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'========================================================================================
'
'   CPicross        Picross.cls
'   ピクロスのゲーム管理
'
'========================================================================================

'ゲームID
Private mlngGameID As Long

'背理法モード
Private mlngTestModeLevel As Long

'フィールド
Private mlngFieldCols As Long
Private mlngFieldRows As Long
Private mlngSquares() As Long

'ヒントデータ
Private mutTateHints() As tPicrossHint       '縦方向のヒント
Private mutYokoHints() As tPicrossHint       '横方向のヒント

'もっとも長いヒントデータ
Private mlngMaxTateHintsLength As Long
Private mlngMaxYokoHintsLength As Long

Private mlngRestSquares As Long

Private Const mlngFieldLeftMargin As Long = 2
Private Const mlngFieldTopMargin As Long = 2
Private mlngSquareWidth As Long
Private mlngSquareHeight As Long

'ヒントを表示するのに必要な領域
Private mlngHintAreaWidth As Long
Private mlngHintAreaHeight As Long

'========================================================================================
'   パブリックメンバ関数（メソッド）
'
Public Function AutoAnswerOneCol(ByVal nCol As Long, _
    ByVal nLevel As Long, ByVal bMsg As Boolean) As Boolean
'------------------------------------------------------------------------------
'指定された(縦方向の)列の解答をする
'------------------------------------------------------------------------------
Dim Y As Long, lngResult As Long
Dim utCurrent As tPicrossLine
Dim utResult As tPicrossLine
Dim lngCount As Long

    'パターン数を計算する
    lngCount = 0
    With mutTateHints(nCol)
        For Y = 0 To .nCount - 1
            lngCount = lngCount + .nNumbers(Y)
        Next Y
        lngCount = lngCount + (.nCount - 1)
        lngCount = mlngFieldRows - lngCount
        
        glngNextShowFrame = 0
        gstrCurrentCursorType = "現在の列 = "
        glngCurrentCursorPos = nCol
        glngLastLinePatterns = glngCurrentPatterns
        glngCurrentPatterns = 0
    End With
    
    '指定された列のデータを収集する
    With utCurrent
        For Y = 0 To mlngFieldRows - 1
            .nSquares(Y) = mlngSquares(nCol, Y)
            If .nSquares(Y) = 0 Then .nSquares(Y) = SQUARE_BLANK
        Next Y
    End With
    
    Select Case nLevel
        Case 1:
            lngResult = stepLineLv1(mlngFieldRows, mutTateHints(nCol), utCurrent, utResult)
        Case Else:
            lngResult = stepLineLv2(mlngFieldRows, mutTateHints(nCol), utCurrent, utResult)
    End Select
    
    If (lngResult < 0) Then
        If (bMsg) Then MsgBox "エラーが発生しました。問題と現在の局面を確かめてください。"
        AutoAnswerOneCol = False
        Exit Function
    End If
    
    '結果を転送する
    With utResult
        For Y = 0 To mlngFieldRows - 1
            If (.nSquares(Y) = SQUARE_BLANK) Then .nSquares(Y) = 0
            If (mlngSquares(nCol, Y) = 0) And (.nSquares(Y) <> 0) Then
                mlngRestSquares = mlngRestSquares - 1
            End If
            mlngSquares(nCol, Y) = .nSquares(Y)
        Next Y
    End With
    
    '正常終了
    AutoAnswerOneCol = True
End Function

Public Function AutoAnswerOneRow(ByVal nRow As Long, _
    ByVal nLevel As Long, ByVal bMsg As Boolean) As Boolean
'------------------------------------------------------------------------------
'指定された(横方向の)行の解答をする
'------------------------------------------------------------------------------
Dim X As Long, lngResult As Long
Dim utCurrent As tPicrossLine
Dim utResult As tPicrossLine
Dim lngCount As Long

    'パターン数を計算する
    lngCount = 0
    With mutYokoHints(nRow)
        For X = 0 To .nCount - 1
            lngCount = lngCount + .nNumbers(X)
        Next X
        lngCount = lngCount + (.nCount - 1)
        lngCount = mlngFieldCols - lngCount
        
        glngNextShowFrame = 0
        gstrCurrentCursorType = "現在の行 = "
        glngCurrentCursorPos = nRow
        glngLastLinePatterns = glngCurrentPatterns
        glngCurrentPatterns = 0
    End With
    
    '指定された行のデータを収集する
    With utCurrent
        For X = 0 To mlngFieldCols - 1
            .nSquares(X) = mlngSquares(X, nRow)
        Next X
    End With
    
    Select Case nLevel
        Case 1:
            lngResult = stepLineLv1(mlngFieldCols, mutYokoHints(nRow), utCurrent, utResult)
        Case Else:
            lngResult = stepLineLv2(mlngFieldCols, mutYokoHints(nRow), utCurrent, utResult)
    End Select
    
    If (lngResult < 0) Then
        If (bMsg) Then MsgBox "エラーが発生しました。問題と現在の局面を確かめてください。"
        AutoAnswerOneRow = False
        Exit Function
    End If
    
    '結果を転送する
    With utResult
        For X = 0 To mlngFieldCols - 1
            If (.nSquares(X) = SQUARE_BLANK) Then .nSquares(X) = 0
            If (mlngSquares(X, nRow) = 0) And (.nSquares(X) <> 0) Then
                mlngRestSquares = mlngRestSquares - 1
            End If
            mlngSquares(X, nRow) = .nSquares(X)
        Next X
    End With
    
    '正常終了
    AutoAnswerOneRow = True
End Function

Public Function ChangeSquare(ByVal nCol As Long, ByVal nRow As Long, ByVal nState As Long) As Long
Dim lngPrev As Long

    lngPrev = mlngSquares(nCol, nRow)
    If (lngPrev And SQUARE_BLANK) Then lngPrev = 0
    
    If (nState And SQUARE_BLANK) Then nState = 0
    
    If (nState = 0) Then
        mlngSquares(nCol, nRow) = 0
        If (lngPrev <> 0) Then mlngRestSquares = mlngRestSquares + 1
    Else
        mlngSquares(nCol, nRow) = (nState And &H2F&)
        If (lngPrev = 0) Then mlngRestSquares = mlngRestSquares - 1
    End If
    
    ChangeSquare = mlngRestSquares
End Function

Public Function DrawCursor(ByRef lpTarget As PictureBox, _
    ByVal nCol As Long, ByVal nRow As Long, ByVal bNoHints As Boolean) As Boolean
'------------------------------------------------------------------------------
'指定されたピクチャーボックスに、カーソルを表示する
'bNoHintsをTrue にするとヒントを表示する為の領域を空けない
'------------------------------------------------------------------------------
Dim DX As Long, DY As Long
Dim lngLeft As Long, lngTop As Long

    '表示する位置を決定する
    If (bNoHints = False) Then
        lngLeft = mlngMaxYokoHintsLength * mlngSquareWidth + mlngFieldLeftMargin
        lngTop = mlngMaxTateHintsLength * mlngSquareHeight + mlngFieldTopMargin
    Else
        lngLeft = 0
        lngTop = 0
    End If
    
    DX = nCol * mlngSquareWidth + lngLeft
    DY = nRow * mlngSquareHeight + lngTop
    lpTarget.Line (DX, DY)-Step(mlngSquareWidth, 2), &HFF&, BF
    lpTarget.Line (DX, DY)-Step(2, mlngSquareHeight), &HFF&, BF
    lpTarget.Line (DX, DY + mlngSquareWidth)-Step(mlngSquareWidth, -2), &HFF&, BF
    lpTarget.Line (DX + mlngSquareWidth, DY)-Step(-2, mlngSquareHeight), &HFF&, BF
End Function

Public Function DrawGameField(ByRef lpTarget As PictureBox, ByVal nMargin As Long, _
    ByVal bNoCheckMarks As Boolean, ByVal bNoHints As Boolean, _
    ByVal nLeftCol As Long, ByVal nTopRow As Long, _
    ByVal nShowCols As Long, ByVal nShowRows As Long) As Boolean
'------------------------------------------------------------------------------
'指定されたピクチャーボックスに、現在の局面を表示する
'bNoHintsをTrue にするとヒントを表示する為の領域を空けない
'------------------------------------------------------------------------------
Dim X As Long, Y As Long, DX As Long, DY As Long
Dim lngLeft As Long, lngTop As Long, lngColor As Long

    '表示する位置を決定する
    If (bNoHints = False) Then
        lngLeft = mlngMaxYokoHintsLength * mlngSquareWidth + mlngFieldLeftMargin
        lngTop = mlngMaxTateHintsLength * mlngSquareHeight + mlngFieldTopMargin
    Else
        lngLeft = 0
        lngTop = 0
    End If
    
    If (nShowCols < 0) Then nShowCols = mlngFieldCols - nLeftCol
    If (nShowRows < 0) Then nShowRows = mlngFieldRows - nTopRow
    
    'これから描画する範囲をクリアする
    X = (nLeftCol * mlngSquareWidth) + lngLeft
    Y = (nTopRow * mlngSquareHeight) + lngTop
    DX = (nShowCols * mlngSquareWidth) + X - 1
    DY = (nShowRows * mlngSquareHeight) + Y - 1
    lpTarget.Line (X, Y)-(DX, DY), &HFFFFFF, BF
    
    '現在のフィールドを描画する
    For Y = 0 To nShowRows - 1
        If (Y + nTopRow >= mlngFieldRows) Then Exit For
        
        For X = 0 To nShowCols - 1
            If (X + nLeftCol >= mlngFieldCols) Then Exit For
            
            DX = (X + nLeftCol) * mlngSquareWidth + lngLeft
            DY = (Y + nTopRow) * mlngSquareHeight + lngTop
            
            lngColor = mlngSquares(X + nLeftCol, Y + nTopRow)
            If (lngColor And SQUARE_CHECK) Then
                If (bNoCheckMarks = False) Then
                    lpTarget.Line (DX + 1, DY + 1)-Step(mlngSquareWidth - 2, mlngSquareHeight - 2), 0
                    lpTarget.Line (DX + 1, DY + mlngSquareHeight - 1)-Step(mlngSquareWidth - 2, 2 - mlngSquareHeight), 0
                End If
            ElseIf ((lngColor And SQUARE_BLANK) = 0) And (lngColor <> 0) Then
                lpTarget.Line (DX + nMargin, DY + nMargin)-Step(mlngSquareWidth - nMargin * 2, mlngSquareHeight - nMargin * 2), 0, BF
            End If
        Next X
    Next Y
End Function

Public Function DrawHintNumbers(ByRef lpTarget As PictureBox) As Boolean
'------------------------------------------------------------------------------
'指定されたピクチャーボックスに、ヒント数字を描画する
'------------------------------------------------------------------------------
Dim X As Long, Y As Long, DX As Long, DY As Long, lngCount As Long
Dim lngLeft As Long, lngTop As Long, lngWidth As Long, lngHeight As Long
Dim lngColor As Long, lngOffset As Long
Dim lngXOffset As Long, lngYOffset As Long
Dim strText As String

    With lpTarget
        lngYOffset = (mlngSquareWidth - .TextHeight("0")) \ 2
    End With
    
    'まず、マスを描画する
    lngLeft = mlngMaxYokoHintsLength * mlngSquareWidth
    lngTop = mlngMaxTateHintsLength * mlngSquareHeight
    lngWidth = mlngFieldCols * mlngSquareWidth + lngLeft
    lngHeight = mlngFieldRows * mlngSquareHeight + lngTop
    
    mlngHintAreaWidth = lngLeft + mlngFieldLeftMargin
    mlngHintAreaHeight = lngTop + mlngFieldTopMargin
    
    lngColor = &H7F7F&
    
    lpTarget.Line (mlngFieldLeftMargin, mlngHintAreaHeight)-Step(lngWidth, 0), lngColor
    lpTarget.Line (mlngHintAreaWidth, mlngFieldTopMargin)-Step(0, lngHeight), lngColor
        
    '横方向のヒントを表示する
    DY = mlngHintAreaHeight
    Y = (mlngFieldRows * mlngSquareHeight)
    For X = 0 To mlngMaxYokoHintsLength - 1
        DX = (X * mlngSquareWidth) + mlngFieldLeftMargin
        If (X And 1) Then lngColor = &HC0C0C0 Else lngColor = &HFFFFFF
        lpTarget.Line (DX, DY)-Step(mlngSquareWidth - 1, Y - 1), lngColor, BF
    Next X
    For Y = 0 To mlngFieldRows
        lngColor = &H0&
        If (Y Mod 5) = 0 Then lngColor = &HFF00&
        DY = (Y * mlngSquareHeight) + mlngHintAreaHeight
        lpTarget.Line (mlngFieldLeftMargin, DY)-Step(lngWidth, 0), lngColor
        
        If Y = mlngFieldRows Then Exit For
        With mutYokoHints(Y)
            lngCount = .nCount
            If (lngCount <= 0) Then lngCount = 1
            lngOffset = mlngMaxYokoHintsLength - lngCount
            
            For X = 0 To lngCount - 1
                strText = Trim$(Str$(.nNumbers(X)))
                With lpTarget
                    lngXOffset = (mlngSquareWidth - .TextWidth(strText)) \ 2
                    .CurrentX = (X + lngOffset) * mlngSquareWidth + mlngFieldTopMargin + lngXOffset
                    .CurrentY = DY + lngYOffset
                End With
                lpTarget.Print strText
            Next X
        End With
    Next Y
    
    '縦方向のヒントを表示する
    DX = mlngHintAreaWidth
    X = (mlngFieldCols * mlngSquareWidth)
    For Y = 0 To mlngMaxTateHintsLength - 1
        DY = (Y * mlngSquareHeight) + mlngFieldTopMargin
        If (Y And 1) Then lngColor = &HC0C0C0 Else lngColor = &HFFFFFF
        lpTarget.Line (DX, DY)-Step(X - 1, mlngSquareHeight - 1), lngColor, BF
    Next Y
    For X = 0 To mlngFieldCols
        lngColor = &H0&
        If (X Mod 5) = 0 Then lngColor = &HFF00&
        DX = (X * mlngSquareWidth) + mlngHintAreaWidth
        lpTarget.Line (DX, mlngFieldTopMargin)-Step(0, lngHeight), lngColor
        
        If X = mlngFieldCols Then Exit For
        With mutTateHints(X)
            lngCount = .nCount
            If (lngCount <= 0) Then lngCount = 1
            lngOffset = mlngMaxTateHintsLength - lngCount
            
            For Y = 0 To lngCount - 1
                strText = Trim$(Str$(.nNumbers(Y)))
                With lpTarget
                    lngXOffset = (mlngSquareWidth - .TextWidth(strText)) \ 2
                    .CurrentX = DX + lngXOffset
                    .CurrentY = (Y + lngOffset) * mlngSquareHeight + mlngFieldTopMargin + lngYOffset
                End With
                lpTarget.Print strText
            Next Y
        End With
    Next X
    
    DrawHintNumbers = True
End Function

Public Function EnterTestMode(ByVal nCursorX As Long, ByVal nCursorY As Long) As Long
'------------------------------------------------------------------------------
'テストモード（背理法モード）に入る
'------------------------------------------------------------------------------
Dim strFileName As String

    '現在の状態をファイルに保存する
    strFileName = gstrAppPath & "\Test" & mlngGameID & "." & Trim$(Str$(mlngTestModeLevel))
    SaveGameStatus Me, strFileName, nCursorX, nCursorY
    
    mlngTestModeLevel = mlngTestModeLevel + 1
End Function

Public Function ExitTestMode(ByVal bFlagSet As Boolean, ByRef lpCursorX As Long, ByRef lpCursorY As Long) As Long
'------------------------------------------------------------------------------
'テストモード（背理法モード）から抜ける
'------------------------------------------------------------------------------
Dim strFileName As String

    If (mlngTestModeLevel = 0) Then
        MsgBox "現在はテストモードではありません！"
        ExitTestMode = 0
        Exit Function
    End If
    
    mlngTestModeLevel = mlngTestModeLevel - 1
    
    If (bFlagSet = False) Then
        '保存された局面をロードする
        strFileName = gstrAppPath & "\Test" & mlngGameID & "." & Trim$(Str$(mlngTestModeLevel))
        If LoadGameStatus(Me, strFileName, lpCursorX, lpCursorY) = False Then
            MsgBox "致命的エラー：テストモードに入る直前の局面を記録したデータをロードできません。" & strFileName
        End If
    End If
    ExitTestMode = mlngTestModeLevel
End Function

Public Function GetScreenHeight() As Long
'------------------------------------------------------------------------------
'フィールドを表示するのに最低限必要な高さを取得する
'------------------------------------------------------------------------------
Dim lngHeight As Long

    lngHeight = (mlngMaxTateHintsLength + mlngFieldRows) * mlngSquareHeight + mlngFieldTopMargin * 2
    GetScreenHeight = lngHeight
End Function

Public Function GetScreenWidth() As Long
'------------------------------------------------------------------------------
'フィールドを表示するのに最低限必要な幅を取得する
'------------------------------------------------------------------------------
Dim lngWidth As Long

    lngWidth = (mlngMaxYokoHintsLength + mlngFieldCols) * mlngSquareWidth + mlngFieldLeftMargin * 2
    GetScreenWidth = lngWidth
End Function

Public Function InitializeGame(ByVal nGameID As Long, ByVal nCols As Long, ByVal nRows As Long, _
    ByVal nSquareWidth As Long, ByVal nSquareHeight As Long) As Boolean
'------------------------------------------------------------------------------
'ゲームを初期化する
'------------------------------------------------------------------------------

    mlngSquareWidth = nSquareWidth
    mlngSquareHeight = nSquareHeight
    
    mlngGameID = nGameID
    mlngFieldCols = nCols
    mlngFieldRows = nRows
    mlngRestSquares = nCols * nRows
    
    ReDim mlngSquares(0 To nCols - 1, 0 To nRows - 1)
    ReDim mutTateHints(0 To nCols - 1)
    ReDim mutYokoHints(0 To nRows - 1)
    
    mlngMaxTateHintsLength = 1
    mlngMaxYokoHintsLength = 1
    
    If (nCols > 40) Or (nRows > 30) Then
        glngShowIntervals = 16383
        glngMinLevel = MAXLEVEL
        glngMaxLevel = MAXLEVEL
    Else
        glngShowIntervals = 255
        glngMinLevel = MAXLEVEL
        glngMaxLevel = MAXLEVEL
    End If
    
    InitializeGame = True
End Function

Public Function PicrossLoadStatus(ByVal lngFileNumber As Long, ByVal lngStartOffset As Long) As Boolean
'------------------------------------------------------------------------------
'ファイルから情報を読み取って、保存されている状態を復元する
'------------------------------------------------------------------------------
Dim lngID As Long, lngCols As Long, lngRows As Long

    Get #lngFileNumber, 1 + lngStartOffset, lngID
    Get #lngFileNumber, , mlngTestModeLevel
    Get #lngFileNumber, , lngCols
    Get #lngFileNumber, , lngRows
    
    If InitializeGame(lngID, lngCols, lngRows, 14, 14) = False Then
        PicrossLoadStatus = False
        Exit Function
    End If
    
    Get #lngFileNumber, , mlngSquares()
    Get #lngFileNumber, , mutTateHints()
    Get #lngFileNumber, , mutYokoHints()
    Get #lngFileNumber, , mlngMaxTateHintsLength
    Get #lngFileNumber, , mlngMaxYokoHintsLength
    Get #lngFileNumber, , mlngRestSquares
    
    PicrossLoadStatus = True
End Function

Public Function PicrossSaveStatus(ByVal lngFileNumber As Long, ByVal lngStartOffset As Long) As Boolean
'------------------------------------------------------------------------------
'ファイルに現在の状態に復元するのに必要なすべての情報を保存する
'------------------------------------------------------------------------------

    Put #lngFileNumber, 1 + lngStartOffset, mlngGameID
    Put #lngFileNumber, , mlngTestModeLevel
    Put #lngFileNumber, , mlngFieldCols
    Put #lngFileNumber, , mlngFieldRows
    Put #lngFileNumber, , mlngSquares()
    Put #lngFileNumber, , mutTateHints()
    Put #lngFileNumber, , mutYokoHints()
    Put #lngFileNumber, , mlngMaxTateHintsLength
    Put #lngFileNumber, , mlngMaxYokoHintsLength
    Put #lngFileNumber, , mlngRestSquares
    
    PicrossSaveStatus = True
End Function

Public Sub SetTateHint(ByVal X As Long, ByVal nLength As Long, ByRef lpNumbers() As Long, ByRef lpColors() As Long)
'------------------------------------------------------------------------------
'指定した列の、縦方向のヒントをセットする
'------------------------------------------------------------------------------
Dim i As Long

    If (nLength > mlngMaxTateHintsLength) Then mlngMaxTateHintsLength = nLength
    
    With mutTateHints(X)
        .nCount = nLength
        If nLength = 0 Then
            .nNumbers(0) = 0
            .nColors(0) = 1
        Else
            For i = 0 To nLength - 1
                .nNumbers(i) = lpNumbers(i)
                .nColors(i) = lpColors(i)
            Next i
        End If
    End With
End Sub

Public Sub SetYokoHint(ByVal Y As Long, ByVal nLength As Long, ByRef lpNumbers() As Long, ByRef lpColors() As Long)
'------------------------------------------------------------------------------
'指定した行の、横方向のヒントをセットする
'------------------------------------------------------------------------------
Dim i As Long

    If (nLength > mlngMaxYokoHintsLength) Then mlngMaxYokoHintsLength = nLength
    
    With mutYokoHints(Y)
        .nCount = nLength
        If nLength = 0 Then
            .nNumbers(0) = 0
            .nColors(0) = 1
        Else
            For i = 0 To nLength - 1
                .nNumbers(i) = lpNumbers(i)
                .nColors(i) = lpColors(i)
            Next i
        End If
    End With
End Sub

'========================================================================================
'   パブリックメンバ関数（プロパティ）
'
Public Property Get FieldCols() As Long
    FieldCols = mlngFieldCols
End Property

Public Property Get FieldRows() As Long
    FieldRows = mlngFieldRows
End Property

Public Property Get GameID() As Long
    GameID = mlngGameID
End Property

Public Property Get HintAreaHeight() As Long
    HintAreaHeight = mlngHintAreaHeight
End Property

Public Property Get HintAreaWidth() As Long
    HintAreaWidth = mlngHintAreaWidth
End Property

Public Property Get RestSquares() As Long
    RestSquares = mlngRestSquares
End Property

Public Property Get SquareHeight() As Long
    SquareHeight = mlngSquareHeight
End Property

Public Property Let SquareHeight(ByVal nNewValue As Long)
    If (nNewValue > 0) Then mlngSquareHeight = nNewValue
End Property

Public Property Get SquareWidth() As Long
    SquareWidth = mlngSquareWidth
End Property

Public Property Let SquareWidth(ByVal nNewValue As Long)
    If (nNewValue > 0) Then mlngSquareWidth = nNewValue
End Property

Public Property Get TestModeLevel() As Long
    TestModeLevel = mlngTestModeLevel
End Property
