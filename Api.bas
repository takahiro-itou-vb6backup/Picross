Attribute VB_Name = "mdlAPI"
Option Explicit
'==============================================================================
'mdlAPI (API.bas)
'Win32 API モジュール
'
'Windows API 関数の宣言と
'それらを使いやすくする関数
'==============================================================================

'設定ファイルの読み書き
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

'画像転送
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Const SRCCOPY = &HCC0020
Public Const SRCAND = &H8800C6

Public Function GetSettingINI(ByVal strFileName As String, ByVal strSection As String, ByVal strKey As String, ByVal strDefault As String) As String
'--------------------------------------------------------------------
'初期化ファイル(.INIファイル)から設定を読み込む
'レジストリ用のGetSettingを初期化ファイル用にしたもの
'--------------------------------------------------------------------
Dim rc As Long
Dim lngPos As Long
Dim strTemp As String

    strTemp = Space$(1024)
    rc = GetPrivateProfileString(strSection, strKey, strDefault, strTemp, Len(strTemp), strFileName)
    lngPos = InStr(strTemp, vbNullChar)
    If lngPos = 0 Then
        'NULL文字がないので、バッファをオーバーした
        GetSettingINI = strTemp
    Else
        'NULL文字の手前までを取り出す
        GetSettingINI = Left$(strTemp, lngPos - 1)
    End If
End Function

Public Sub SaveSettingINI(ByVal strFileName As String, ByVal strSection As String, ByVal strKey As String, ByVal strData As String)
'--------------------------------------------------------------------
'初期化ファイル(.INIファイル)に設定を書き込む
'レジストリに用のSaveSettingを初期化ファイル用にしたもの
'--------------------------------------------------------------------
Dim rc As Long

    rc = WritePrivateProfileString(strSection, strKey, strData, strFileName)
End Sub
