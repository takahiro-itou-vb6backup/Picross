VERSION 5.00
Begin VB.Form frmView 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "縮小表示"
   ClientHeight    =   3195
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   4680
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows の既定値
   Begin VB.PictureBox picView 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      Height          =   495
      Left            =   120
      ScaleHeight     =   33
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   81
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Menu mnuSize 
      Caption         =   "マスのサイズ(&S)"
      Begin VB.Menu mnuSizeDefault 
         Caption         =   "16x16"
         Index           =   0
      End
      Begin VB.Menu mnuSizeDefault 
         Caption         =   "8x8"
         Index           =   1
      End
      Begin VB.Menu mnuSizeSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSizeCustom 
         Caption         =   "カスタム(&C)"
      End
   End
End
Attribute VB_Name = "frmView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mobjTempGame As New CPicross

Private mlngSquareWidth As Long
Private mlngSquareHeight As Long

Public Sub SetViewSize(ByVal nWidth As Long, ByVal nHeight As Long, ByVal nCols As Long, ByVal nRows As Long)
Dim lngViewWidth As Long
Dim lngViewHeight As Long

    With Screen
        lngViewWidth = nWidth * nCols * .TwipsPerPixelX
        lngViewHeight = nHeight * nRows * .TwipsPerPixelY
    End With
    
    picView.Move 120, 120, lngViewWidth, lngViewHeight
    With Me
        .Width = lngViewWidth + 240 + (.Width - .ScaleWidth)
        .Height = lngViewHeight + 240 + (.Height - .ScaleHeight)
    End With
End Sub

Public Sub UpdateFieldView(ByVal strTempFileName As String)
Dim X As Long, Y As Long

    If LoadGameStatus(mobjTempGame, strTempFileName, X, Y) = False Then
        Stop
    End If
    
    picView.Cls
    With mobjTempGame
        .SquareWidth = mlngSquareWidth
        .SquareHeight = mlngSquareHeight
        SetViewSize mlngSquareWidth, mlngSquareHeight, .FieldCols, .FieldRows
        .DrawGameField picView, 0, True, True, 0, 0, -1, -1
    End With
    picView.Refresh
End Sub

Private Sub Form_Load()
    mlngSquareWidth = 4
    mlngSquareHeight = 4
End Sub

Private Sub mnuSizeDefault_Click(Index As Integer)
    Select Case Index
        Case 0:
            mlngSquareWidth = 16
            mlngSquareHeight = 16
        Case 1:
            mlngSquareWidth = 8
            mlngSquareHeight = 8
        Case Else:
            mlngSquareWidth = 18
            mlngSquareHeight = 18
    End Select
End Sub
