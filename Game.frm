VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmGame 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ゲーム"
   ClientHeight    =   4530
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   6465
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "FixedSys"
      Size            =   13.5
      Charset         =   128
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   6465
   StartUpPosition =   2  '画面の中央
   Begin VB.PictureBox picSquare 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      Height          =   3255
      Left            =   6240
      ScaleHeight     =   217
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   385
      TabIndex        =   5
      Top             =   4560
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.PictureBox picCanvas 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      Height          =   3255
      Left            =   6480
      ScaleHeight     =   217
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   385
      TabIndex        =   4
      Top             =   840
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.VScrollBar vsbTopRow 
      Height          =   3255
      LargeChange     =   10
      Left            =   120
      Max             =   200
      TabIndex        =   3
      Top             =   840
      Width           =   255
   End
   Begin VB.HScrollBar hsbLeftCol 
      Height          =   255
      LargeChange     =   10
      Left            =   360
      Max             =   200
      TabIndex        =   2
      Top             =   600
      Width           =   5535
   End
   Begin MSComDlg.CommonDialog cdlFile 
      Left            =   6600
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox picScreen 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      Height          =   3255
      Left            =   360
      ScaleHeight     =   217
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   385
      TabIndex        =   0
      Top             =   840
      Width           =   5775
   End
   Begin VB.PictureBox picHint 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "ＭＳ ゴシック"
         Size            =   9
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   120
      ScaleHeight     =   217
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   385
      TabIndex        =   1
      Top             =   4560
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.Label lblRest 
      BackStyle       =   0  '透明
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   6015
   End
   Begin VB.Menu mnuGame 
      Caption         =   "ゲーム(&G)"
      Begin VB.Menu mnuGameOpen 
         Caption         =   "問題を開く(&O)"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuGameSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuGameSave 
         Caption         =   "局面をセーブ(&S)"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuGameLoad 
         Caption         =   "局面をロード(&L)"
         Shortcut        =   ^L
      End
      Begin VB.Menu mnuGameSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuGameExit 
         Caption         =   "終了(&X)"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu mnuShow 
      Caption         =   "表示(&S)"
      Begin VB.Menu mnuShowNormal 
         Caption         =   "通常表示(&N)"
      End
      Begin VB.Menu mnuShowRow 
         Caption         =   "一行（→)を折り返し表示(&R)"
      End
      Begin VB.Menu mnuShowCol 
         Caption         =   "一列(↓)を折り返し表示(&C)"
      End
   End
   Begin VB.Menu mnuTest 
      Caption         =   "テストモード(&T)"
      Begin VB.Menu mnuTestStart 
         Caption         =   "新しいレベルを開始(&S)"
         Shortcut        =   {F8}
      End
      Begin VB.Menu mnuTestExit 
         Caption         =   "現在のレベルを終了(&E)"
         Shortcut        =   +{F8}
      End
      Begin VB.Menu mnuTestSetAndExitAll 
         Caption         =   "内容を設定して全レベルを終了(&A)"
      End
      Begin VB.Menu mnuTestSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTestCont 
         Caption         =   "黒マスと仮定して続行"
         Index           =   0
      End
      Begin VB.Menu mnuTestCont 
         Caption         =   "白マス(×)と仮定して続行"
         Index           =   1
      End
   End
   Begin VB.Menu mnuAnswer 
      Caption         =   "自動解答(&A)"
      Begin VB.Menu mnuAnswerRow 
         Caption         =   "現在の行(→)"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuAnswerCol 
         Caption         =   "現在の列(↓)"
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuAnswerRows 
         Caption         =   "すべての行"
      End
      Begin VB.Menu mnuAnswerCols 
         Caption         =   "すべての列"
      End
      Begin VB.Menu mnuAnswerAll 
         Caption         =   "すべての行と列"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuAnswerEnd 
         Caption         =   "可能な限り"
         Shortcut        =   +{F5}
      End
      Begin VB.Menu mnuAnswerSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAnswerShow 
         Caption         =   "ステップ表示(&S)"
         Begin VB.Menu mnuAnswerShowStep 
            Caption         =   "表示しない(&N)"
            Index           =   0
         End
         Begin VB.Menu mnuAnswerShowStep 
            Caption         =   "１ステップ毎(&S)"
            Index           =   1
         End
         Begin VB.Menu mnuAnswerShowStep 
            Caption         =   "１ライン毎(&L)"
            Index           =   2
         End
         Begin VB.Menu mnuAnswerShowStep 
            Caption         =   "すべて(&A)"
            Index           =   3
         End
      End
   End
End
Attribute VB_Name = "frmGame"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'ゲーム
Private mblnFlagGameRunning As Boolean
Private mobjGame As New CPicross

'カーソル
Private mlngCursorX As Long
Private mlngCursorY As Long

'自動解答のときのステップ表示
Private mlngShowStep As Long

'最後に使用したダイアログのモード
Private mlngLastUseDialog As Long
Private Const LASTUSEDLGGAME = 0
Private Const LASTUSEDLGSTATE = 1

'表示方法
Private mlngShowMode As Long

'縮小表示
Private mfrmView As New frmView

Public Function AutoAnswer(ByVal nStartRow As Long, ByVal nEndRow As Long, _
    ByVal nStartCol As Long, ByVal nEndCol As Long, ByVal bMessage As Boolean, _
    ByRef lplngStep As Long, Optional ByVal nMaxSteps As Long = -1) As Boolean
'------------------------------------------------------------------------------
'自動解答
'指定された範囲の行および列に対して、自動解答を行う
'範囲にマイナスを指定すれば、フィールドの幅が加算される
'すなわち、-1を指定すれば、フィールドの右端／下端を指定できる。
'lplngStepにステップ数を返す
'nMaxStepsにマイナスを指定すれば、ステップ回数に上限をつけない
'------------------------------------------------------------------------------
Dim lngLevel As Long, lngPrev As Long, lngCur As Long
Dim lngStep As Long
Dim lngSaveX As Long, lngSaveY As Long, lngSaveShow As Long
Dim blnError As Boolean

    lngStep = 0
    lngSaveX = mlngCursorX
    lngSaveY = mlngCursorY
    lngSaveShow = mlngShowMode
    
    With mobjGame
        If (nStartRow < 0) Then nStartRow = nStartRow + .FieldRows
        If (nStartCol < 0) Then nStartCol = nStartCol + .FieldCols
        If (nEndRow < 0) Then nEndRow = nEndRow + .FieldRows
        If (nEndCol < 0) Then nEndCol = nEndCol + .FieldCols
        
        lngPrev = .FieldCols * .FieldRows + 1
        lngCur = .RestSquares
        
        For lngLevel = glngMinLevel To glngMaxLevel
            Do While (lngCur > 0)
                DoEvents
                blnError = False
                If (lngCur = lngPrev) Then lngLevel = lngLevel + 1
                
                '横方向
                If (mlngShowMode <> 0) Then mlngShowMode = 1
                mlngCursorX = nStartCol
                For mlngCursorY = nStartRow To nEndRow
                    If .AutoAnswerOneRow(mlngCursorY, lngLevel, bMessage) = False Then
                        blnError = True
                        Exit Do
                    End If
                    If (mlngShowStep >= 2) Then UpdateGameScreen 0, mlngCursorY, -1, 1
                    If (mlngShowStep >= 3) Then UpdateSmallView
                    DoEvents
                Next mlngCursorY
                
                '縦方向
                If (mlngShowMode <> 0) Then mlngShowMode = 2
                mlngCursorY = nStartRow
                For mlngCursorX = nStartCol To nEndCol
                    If .AutoAnswerOneCol(mlngCursorX, lngLevel, bMessage) = False Then
                        blnError = True
                        Exit Do
                    End If
                    If (mlngShowStep >= 2) Then UpdateGameScreen mlngCursorX, 0, 1, -1
                    If (mlngShowStep >= 3) Then UpdateSmallView
                    DoEvents
                Next mlngCursorX
                
                '次のステップの準備
                lngPrev = lngCur
                lngCur = .RestSquares
                If (lngPrev = lngCur) Then Exit Do
                
                lngStep = lngStep + 1
                If (mlngShowStep >= 1) Then UpdateGameScreen 0, 0, -1, -1
                If (mlngShowStep >= 3) Then UpdateSmallView
                
                If (nMaxSteps > 0) And (lngStep >= nMaxSteps) Then Exit Do
            Loop
            
            SaveAutoAnswerFinishLevel lngLevel
        Next lngLevel
    End With
            
    If (blnError = False) Then
        mlngCursorX = lngSaveX
        mlngCursorY = lngSaveY
    End If
    Select Case lngSaveShow
        Case 1: mnuShowRow_Click
        Case 2: mnuShowCol_Click
        Case Else: mnuShowNormal_Click
    End Select
    
    'ステップ数を返す
    lplngStep = lngStep
    
    '正常終了／エラーを返す
    AutoAnswer = (Not blnError)
End Function

Public Function AutoAnswerHairihou(ByVal nCol As Long, ByVal nRow As Long, _
    ByVal nColor As Long, ByVal nLevel As Long) As Boolean
'------------------------------------------------------------------------------
'背理法に挑戦する
'位置(nCol,nRow)にnColorを置く。失敗したら、もう一方を挑戦する
'------------------------------------------------------------------------------
Dim lngStep As Long

    '現在の状態を取得する
    With mobjGame
    End With
    
    'テストモード開始
    mnuTestStart_Click
    
    ModifyOneSquare nCol, nRow, nColor
    If AutoAnswer(0, -1, 0, -1, False, lngStep, -1) = False Then
        'テストモードを終了
        mnuTestExit_Click
        
        If (nColor And SQUARE_CHECK) Then
            ModifyOneSquare nCol, nRow, 1
            MsgBox "背理法完了。×にして矛盾が出たので、黒マスに確定しました。"
        Else
            ModifyOneSquare nCol, nRow, SQUARE_CHECK
            MsgBox "背理法完了。黒マスにして矛盾が出たので、×（白マス）に確定しました。"
        End If
    Else
        MsgBox "背理法失敗。矛盾が発生しませんでした・・・"
    End If
    
    AutoAnswerHairihou = True
End Function

Public Sub ChangeSize()
'------------------------------------------------------------------------------
'現在の問題にあわせて、ウィンドウのサイズを変更する
'------------------------------------------------------------------------------
Dim lngScaleWidth As Long, lngScaleHeight As Long
Dim lngFieldCols As Long, lngFieldRows As Long
Dim lngFieldWidth As Long, lngFieldHeight As Long
Dim lngScreenPixelWidth As Long
Dim lngScreenPixelHeight As Long
Dim lngScreenWidth As Long, lngScreenHeight As Long
Dim lngFormWidth As Long

    With Screen
        lngScaleWidth = .TwipsPerPixelX
        lngScaleHeight = .TwipsPerPixelY
    End With
    
    If (mblnFlagGameRunning = False) Then
        lngScreenWidth = 480 * lngScaleWidth
        lngScreenHeight = 360 * lngScaleHeight
    Else
        With mobjGame
            lngFieldCols = .FieldCols
            lngFieldRows = .FieldRows
            
            lngFieldWidth = lngFieldCols * .SquareWidth * lngScaleWidth
            lngFieldHeight = lngFieldRows * .SquareHeight * lngScaleHeight
            
            lngScreenPixelWidth = .GetScreenWidth()
            lngScreenPixelHeight = .GetScreenHeight()
            lngScreenWidth = lngScreenPixelWidth * lngScaleWidth
            lngScreenHeight = lngScreenPixelHeight * lngScaleHeight
            
            'ヒント表示用
            With picHint
                .Width = lngScreenWidth
                .Height = lngScreenHeight
                .Cls
            End With
            
            .DrawHintNumbers picHint
        End With
        
        'セル用のオフスクリーンイメージ
        With picSquare
            .Width = lngFieldWidth
            .Height = lngFieldHeight
        End With
        
        '描画作業用の一時バッファ
        With picCanvas
            .Width = lngScreenWidth
            .Height = lngScreenHeight
        End With
    End If
    
    If (lngScreenPixelWidth >= 800) Then
        lngScreenPixelWidth = 800
        lngScreenWidth = lngScaleWidth * 800
    End If
    
    If (lngScreenPixelHeight >= 480) Then
        lngScreenPixelHeight = 600
        lngScreenHeight = lngScaleHeight * 600
    End If
    
    With picScreen
        .Width = lngScreenWidth
        .Height = lngScreenHeight
    End With
    
    With Me
        lngFormWidth = 480 + lngScreenWidth + (.Width - .ScaleWidth)
        If (lngFormWidth < 4800) Then lngFormWidth = 4800
        
        .Width = lngFormWidth
        .Height = 1080 + lngScreenHeight + (.Height - .ScaleHeight)
        .Move (Screen.Width - .Width) \ 2, (Screen.Height - .Height) \ 2
    End With
End Sub

Private Sub CopySquaresToCanvas(ByVal bWithHint As Boolean, _
    ByVal nDestX As Long, ByVal nDestY As Long, _
    ByVal nLeft As Long, ByVal nTop As Long, ByVal nCols As Long, ByVal nRows As Long)
'------------------------------------------------------------------------------
'指定した範囲のマスの内容をキャンバスにコピーする
'------------------------------------------------------------------------------
Dim lngSquareWidth As Long, lngSquareHeight As Long
Dim SX As Long, SY As Long, MX As Long, MY As Long
Dim lngWidth As Long, lngHeight As Long
Dim lngDest As Long
Dim lngResult As Long

    lngDest = picCanvas.hDC
    With mobjGame
        lngSquareWidth = .SquareWidth
        lngSquareHeight = .SquareHeight
        MX = .HintAreaWidth
        MY = .HintAreaHeight
    End With
    
    SX = nLeft * lngSquareWidth
    SY = nTop * lngSquareHeight
    lngWidth = (nCols * lngSquareWidth) + 1
    lngHeight = (nRows * lngSquareHeight) + 1
    
    If (bWithHint = False) Then
        lngResult = BitBlt(lngDest, nDestX, nDestY, lngWidth, lngHeight, picHint.hDC, SX + MX, SY + MY, SRCCOPY)
        lngResult = BitBlt(lngDest, nDestX, nDestY, lngWidth, lngHeight, picSquare.hDC, SX, SY, SRCAND)
    Else
    End If
End Sub

Public Sub EnableAutoAnswerMenu(ByVal bEnable As Boolean)
'------------------------------------------------------------------------------
'自動解答に関するメニューを設定する
'------------------------------------------------------------------------------
    mnuAnswer.Enabled = bEnable
    
    If (bEnable = True) And (Not (gobjfProgress Is Nothing)) Then
        Unload gobjfProgress
        Set gobjfProgress = Nothing
    End If
End Sub

Public Sub EnableTestModeMenu()
'------------------------------------------------------------------------------
'テストモードに関するメニューを設定する
'------------------------------------------------------------------------------
Dim blnExit As Boolean

    If (mobjGame.TestModeLevel = 0) Then
        blnExit = False
    Else
        blnExit = True
    End If
    
    mnuTestExit.Enabled = blnExit
    mnuTestSetAndExitAll.Enabled = blnExit
End Sub

Public Sub ModifyOneSquare(ByVal nCol As Long, ByVal nRow As Long, ByVal nNewValue As Byte)
'------------------------------------------------------------------------------
'指定した位置の状態を変更する
'------------------------------------------------------------------------------

    With mobjGame
        .ChangeSquare nCol, nRow, nNewValue
        .DrawGameField picSquare, 2, False, True, nCol, nRow, 1, 1
    End With
End Sub

Public Sub ProcessOpenGame(ByVal strFileName As String)
'------------------------------------------------------------------------------
'問題データをロードする
'------------------------------------------------------------------------------

    If LoadGameData(mobjGame, strFileName) = False Then
        MsgBox "問題をロードできません。" & vbCrLf & strFileName
        mblnFlagGameRunning = False
        Exit Sub
    End If
    
    With mobjGame
'        If (.FieldCols > 40) Then
            mnuShowRow.Enabled = True
'        Else
'            mnuShowRow.Enabled = False
'        End If
        
'        If (.FieldRows > 30) Then
            mnuShowCol.Enabled = True
'        Else
'            mnuShowCol.Enabled = False
'        End If
        
        mnuShowNormal.Enabled = True
        mnuShowNormal_Click
    End With
    
    SetGameRunningFlag True
    EnableTestModeMenu
    ChangeSize
    UpdateGameScreen 0, 0, -1, -1
    UpdateSmallView
    
    Me.SetFocus
End Sub

Public Sub SaveAutoAnswerFinishLevel(ByVal nLevel As Long)
Dim strFileName As String

    strFileName = gstrAppPath & "\AAS" & mobjGame.GameID & "." & nLevel & ".psv"
    If SaveGameStatus(mobjGame, strFileName, mlngCursorX, mlngCursorY) = False Then
        MsgBox "致命的エラー：セーブに失敗しました。再試行するか別のファイルに保存してください。"
    End If
End Sub

Public Sub SetGameRunningFlag(ByVal bRunning As Boolean)
'------------------------------------------------------------------------------
'ゲームのフラグを更新する
'同時に、メニューを更新する
'------------------------------------------------------------------------------

    mblnFlagGameRunning = bRunning
'    mnuGameOpen.Enabled = Not (bRunning)
    mnuShow.Enabled = bRunning
    mnuTest.Enabled = bRunning
    mnuAnswer.Enabled = bRunning
    
    If (bRunning) Then
        mfrmView.Show
    Else
        Unload mfrmView
    End If
End Sub

Private Sub UpdateGameScreen(ByVal nRedrawLeftCol As Long, ByVal nRedrawTopRow As Long, _
    ByVal nRedrawCols As Long, ByVal nRedrawRows As Long)
'------------------------------------------------------------------------------
'ゲームスクリーンを更新する
'------------------------------------------------------------------------------
Dim lngLevel As Long
Dim lngResult As Long
Dim lngLeft As Long, lngTop As Long
Dim X As Long, Y As Long, W As Long, H As Long, lngRest As Long
Dim lngLeftCol As Long, lngTopRow As Long
Dim lngFieldCols As Long, lngFieldRows As Long
Dim lngSquareWidth As Long, lngSquareHeight As Long
Dim lngHintWidth As Long, lngHintHeight As Long
Dim strText As String, strTemp As String
Dim strShowPos As String, strCursorPos As String

    If (mblnFlagGameRunning = False) Then
        strText = "F2 キーを押して、問題を選んでください。"
        With picScreen
            .Cls
            .CurrentX = (.ScaleWidth - .TextWidth(strText)) \ 2
            .CurrentY = (.ScaleHeight - .TextHeight(strText)) \ 2
            picScreen.Print strText
            .Refresh
        End With
        Exit Sub
    End If
    
    strText = "残り" & mobjGame.RestSquares & "マス"
    lblRest.Caption = strText
    
    With mobjGame
        lngLeft = .HintAreaWidth
        lngTop = .HintAreaHeight
        lngFieldCols = .FieldCols
        lngFieldRows = .FieldRows
        lngSquareWidth = .SquareWidth
        lngSquareHeight = .SquareHeight
        lngHintWidth = .HintAreaWidth
        lngHintHeight = .HintAreaHeight
                
        If (.RestSquares = 0) Then UpdateSmallView
        If (nRedrawCols = -1) And (nRedrawRows = -1) Then picSquare.Cls
        If (nRedrawCols <> 0) And (nRedrawRows <> 0) Then
            .DrawGameField picSquare, 2, False, True, nRedrawLeftCol, nRedrawTopRow, nRedrawCols, nRedrawRows
        End If
    End With
    
    If (mlngShowMode = 1) Then
        With picCanvas
            .Cls
            
            'ヒントデータのコピー
            lngResult = BitBlt(.hDC, 0, 0, lngHintWidth + 1, lngSquareHeight + 1, _
                picHint.hDC, 0, mlngCursorY * lngSquareHeight + lngTop, SRCCOPY)
            W = lngHintWidth + 1
            If (W < lngSquareWidth * 40) Then W = lngSquareWidth * 40 + 1
            
            'マスの描画
            For X = 0 To lngFieldCols - 1 Step 40
                lngRest = lngFieldCols - X
                If (lngRest > 40) Then lngRest = 40
                Y = ((X \ 40) * 3 + 3) * lngSquareHeight
                
                CopySquaresToCanvas False, 0, Y, X, mlngCursorY, lngRest, 1
                
                strTemp = Trim$(Str$(X + lngRest))
                .CurrentX = lngRest * lngSquareWidth + 8
                .CurrentY = Y + (lngSquareHeight - .TextHeight(strTemp)) \ 2
                picCanvas.Print strTemp
                
                H = Y + lngSquareHeight + 1
            Next X
            
            'カーソルの描画
            X = (mlngCursorX Mod 40)
            Y = (mlngCursorX \ 40) * 3 + 3
            mobjGame.DrawCursor picCanvas, X, Y, True
            
            '表示位置
            strShowPos = "表示：" & mlngCursorY & "行"
        End With
        X = 0
        Y = 0
        
    ElseIf (mlngShowMode = 2) Then
        With picCanvas
            .Cls
            
            'ヒントデータのコピー
            lngResult = BitBlt(.hDC, 0, 0, lngSquareWidth + 1, lngHintHeight + 1, _
                picHint.hDC, mlngCursorX * lngSquareWidth + lngLeft, 0, SRCCOPY)
            H = lngHintHeight + 1
            If (H < lngSquareHeight * 30) Then H = lngSquareHeight * 30 + 1
            
            'マスの描画
            For Y = 0 To lngFieldRows - 1 Step 30
                lngRest = lngFieldRows - Y
                If (lngRest > 30) Then lngRest = 30
                X = ((Y \ 30) * 3 + 3) * lngSquareWidth
                
                CopySquaresToCanvas False, X, 0, mlngCursorX, Y, 1, lngRest
                
                strTemp = Trim$(Str$(Y + lngRest))
                .CurrentX = X + (lngSquareWidth - .TextWidth(strTemp)) \ 2
                .CurrentY = lngRest * lngSquareHeight + 8
                picCanvas.Print strTemp
                
                W = X + lngSquareWidth + 1
            Next Y
            
            'カーソルの描画
            X = (mlngCursorY \ 30) * 3 + 3
            Y = (mlngCursorY Mod 30)
            mobjGame.DrawCursor picCanvas, X, Y, True
            
            '表示位置
            strShowPos = "表示：" & mlngCursorX & "列"
        End With
    Else
        lngLeftCol = hsbLeftCol.Value
        lngTopRow = vsbTopRow.Value
        X = lngLeftCol * lngSquareWidth
        Y = lngTopRow * lngSquareHeight
        W = picScreen.ScaleWidth
        H = picScreen.ScaleHeight
        
        With picCanvas
            .Cls
            lngResult = BitBlt(.hDC, 0, 0, W, H, picHint.hDC, X, Y, SRCCOPY)
            lngResult = BitBlt(.hDC, lngLeft, lngTop, W, H, picSquare.hDC, X, Y, SRCAND)
        End With
        mobjGame.DrawCursor picCanvas, mlngCursorX - lngLeftCol, mlngCursorY - lngTopRow, False
        
        '表示位置
        strShowPos = "表示：(" & lngLeftCol & "," & lngTopRow & ")"
    End If
    
    '画像を転送
    With picScreen
        .Cls
        lngResult = BitBlt(.hDC, 0, 0, W, H, picCanvas.hDC, X, Y, SRCCOPY)
        .Refresh
    End With
    
    With mobjGame
        lngLevel = .TestModeLevel
        strCursorPos = "カーソル：(" & mlngCursorX & "," & mlngCursorY & ")"
        If (lngLevel > 0) Then
            strTemp = "　[テストモード　Level: " & lngLevel & "]"
            strText = "ゲーム" & strTemp
        Else
            strText = "ゲーム"
        End If
        strText = strText & "　" & strCursorPos & "　" & strShowPos
        Me.Caption = strText
    End With
End Sub

Private Sub UpdateSmallView()
'------------------------------------------------------------------------------
'縮小表示を更新する
'------------------------------------------------------------------------------
Dim strTemp As String

    strTemp = gstrAppPath & "\~Smlv"
    SaveGameStatus mobjGame, strTemp, 0, 0
    mfrmView.UpdateFieldView strTemp
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If (mblnFlagGameRunning = False) Then
        Exit Sub
    End If
    
    With mobjGame
        Select Case KeyCode
            Case vbKeyA:
                ModifyOneSquare mlngCursorX, mlngCursorY, 1
            Case vbKeyB:
                ModifyOneSquare mlngCursorX, mlngCursorY, SQUARE_CHECK
            Case vbKeyBack, vbKeyEscape
                ModifyOneSquare mlngCursorX, mlngCursorY, 0
                
            Case vbKeyLeft:
                If (mlngCursorX > 0) Then
                    mlngCursorX = mlngCursorX - 1
                Else
                    Exit Sub
                End If
            Case vbKeyUp:
                If (mlngCursorY > 0) Then
                    mlngCursorY = mlngCursorY - 1
                Else
                    Exit Sub
                End If
            Case vbKeyRight:
                If (mlngCursorX < .FieldCols - 1) Then
                    mlngCursorX = mlngCursorX + 1
                Else
                    Exit Sub
                End If
            Case vbKeyDown:
                If (mlngCursorY < .FieldRows - 1) Then
                    mlngCursorY = mlngCursorY + 1
                Else
                    Exit Sub
                End If
        End Select
        
    End With
    
    UpdateGameScreen mlngCursorX, mlngCursorY, 0, 0
End Sub

Private Sub Form_Load()
'------------------------------------------------------------------------------
'ゲームを初期化する
'------------------------------------------------------------------------------

    mnuAnswerShowStep_Click 3
    SetGameRunningFlag False
    ChangeSize
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub Form_Unload(Cancel As Integer)
'------------------------------------------------------------------------------
'プログラムを終了する
'------------------------------------------------------------------------------
    End
End Sub

Private Sub hsbLeftCol_Change()
    UpdateGameScreen 0, 0, 0, 0
End Sub

Private Sub mnuAnswerAll_Click()
'------------------------------------------------------------------------------
'「自動解答」−「すべての行と列」
'------------------------------------------------------------------------------
Dim lngCount As Long
Dim lngLevel As Long, lngPrev As Long, lngCur As Long
Dim lngSaveX As Long, lngSaveY As Long, lngSaveShow As Long
Dim blnMessage As Boolean

    gblnCancel = False
    EnableAutoAnswerMenu False
    lngSaveX = mlngCursorX
    lngSaveY = mlngCursorY
    lngSaveShow = mlngShowMode
    
    With mobjGame
        lngPrev = .RestSquares
        
        For lngLevel = glngMinLevel To glngMaxLevel
            blnMessage = True
            
            '横方向
            If (mlngShowMode <> 0) Then mlngShowMode = 1
            lngCount = .FieldRows
            mlngCursorX = 0
            For mlngCursorY = 0 To lngCount - 1
                If .AutoAnswerOneRow(mlngCursorY, lngLevel, blnMessage) = False Then
                    Exit For
                End If
                If (mlngShowStep >= 2) Then UpdateGameScreen 0, mlngCursorY, -1, 1
                If (mlngShowStep >= 3) Then UpdateSmallView
                DoEvents
            Next mlngCursorY
            
            '縦方向
            If (mlngShowMode <> 0) Then mlngShowMode = 2
            lngCount = .FieldCols
            mlngCursorY = 0
            For mlngCursorX = 0 To lngCount - 1
                If .AutoAnswerOneCol(mlngCursorX, lngLevel, blnMessage) = False Then
                    Exit For
                End If
                If (mlngShowStep >= 2) Then UpdateGameScreen mlngCursorX, 0, 1, -1
                If (mlngShowStep >= 3) Then UpdateSmallView
                DoEvents
            Next mlngCursorX
            
            lngCur = .RestSquares
            If (lngCur < lngPrev) Then Exit For
            DoEvents
            
            SaveAutoAnswerFinishLevel lngLevel
        Next lngLevel
        
    End With
    
    EnableAutoAnswerMenu True
    mlngCursorX = lngSaveX
    mlngCursorY = lngSaveY
    
    Select Case lngSaveShow
        Case 1: mnuShowRow_Click
        Case 2: mnuShowCol_Click
        Case Else: mnuShowNormal_Click
    End Select
        
    UpdateGameScreen 0, 0, -1, -1
    UpdateSmallView
    
    MsgBox "完了"
End Sub

Private Sub mnuAnswerCol_Click()
'------------------------------------------------------------------------------
'「自動解答」−「現在の列(↓)」
'------------------------------------------------------------------------------
Dim lngLevel As Long, lngPrev As Long, lngCur As Long

    gblnCancel = False
    EnableAutoAnswerMenu False
    
    With mobjGame
        lngPrev = .RestSquares
        
        For lngLevel = glngMinLevel To glngMaxLevel
            If .AutoAnswerOneCol(mlngCursorX, lngLevel, True) = False Then
                Exit For
            End If
            
            lngCur = .RestSquares
            If (lngCur < lngPrev) Then Exit For
            DoEvents
        Next lngLevel
    End With
    
    EnableAutoAnswerMenu True

    UpdateGameScreen mlngCursorX, 0, 1, -1
    UpdateSmallView
End Sub

Private Sub mnuAnswerCols_Click()
'------------------------------------------------------------------------------
'「自動解答」−「すべての列」
'------------------------------------------------------------------------------
Dim lngCount As Long
Dim lngLevel As Long, lngPrev As Long, lngCur As Long
Dim lngSaveX As Long, lngSaveY As Long

    gblnCancel = False
    EnableAutoAnswerMenu False
    lngSaveX = mlngCursorX
    lngSaveY = mlngCursorY
    
    With mobjGame
        lngCount = .FieldCols
        lngPrev = .RestSquares
        
        For lngLevel = glngMinLevel To glngMaxLevel
            mlngCursorY = 0
            For mlngCursorX = 0 To lngCount - 1
                If .AutoAnswerOneCol(mlngCursorX, lngLevel, True) = False Then
                    Exit For
                End If
                If (mlngShowStep >= 2) Then UpdateGameScreen mlngCursorX, 0, 1, -1
                If (mlngShowStep >= 3) Then UpdateSmallView
                DoEvents
            Next mlngCursorX
            
            lngCur = .RestSquares
            If (lngCur < lngPrev) Then Exit For
            DoEvents
        Next lngLevel
    End With
    
    EnableAutoAnswerMenu True
    mlngCursorX = lngSaveX
    mlngCursorY = lngSaveY
    UpdateGameScreen 0, 0, -1, -1
    UpdateSmallView
    
    MsgBox "完了"
End Sub

Private Sub mnuAnswerEnd_Click()
'------------------------------------------------------------------------------
'「自動解答」−「可能な限り」
'------------------------------------------------------------------------------
Dim blnResult As Boolean
Dim lngStep As Long
Dim sglStart As Single, sglEnd As Single

    gblnCancel = False
    EnableAutoAnswerMenu False
    
    sglStart = Timer
    blnResult = AutoAnswer(0, -1, 0, -1, True, lngStep, -1)
    sglEnd = Timer
    
    EnableAutoAnswerMenu True
    UpdateGameScreen 0, 0, -1, -1
    UpdateSmallView
    
    MsgBox "計算完了（可能な限り）。" & vbCrLf & "所要時間：" & sglEnd - sglStart & "秒" & vbCrLf & "ステップ：" & lngStep
End Sub

Private Sub mnuAnswerRow_Click()
'------------------------------------------------------------------------------
'「自動解答」−「現在の行(→)」
'------------------------------------------------------------------------------
Dim lngLevel As Long, lngPrev As Long, lngCur As Long

    gblnCancel = False
    EnableAutoAnswerMenu False
    
    With mobjGame
        lngPrev = .RestSquares
        
        For lngLevel = glngMinLevel To glngMaxLevel
            If .AutoAnswerOneRow(mlngCursorY, lngLevel, True) = False Then
                Exit For
            End If
            
            lngCur = .RestSquares
            If (lngCur < lngPrev) Then Exit For
            DoEvents
        Next lngLevel
    End With
    
    EnableAutoAnswerMenu True
    
    UpdateGameScreen 0, mlngCursorY, -1, 1
    UpdateSmallView
End Sub

Private Sub mnuAnswerRows_Click()
'------------------------------------------------------------------------------
'「自動解答」−「すべての行」
'------------------------------------------------------------------------------
Dim lngCount As Long
Dim lngLevel As Long, lngPrev As Long, lngCur As Long
Dim lngSaveX As Long, lngSaveY As Long

    gblnCancel = False
    EnableAutoAnswerMenu False
    
    lngSaveX = mlngCursorX
    lngSaveY = mlngCursorY
    
    With mobjGame
        lngCount = .FieldRows
        lngPrev = .RestSquares
        
        For lngLevel = glngMinLevel To glngMaxLevel
            mlngCursorX = 0
            For mlngCursorY = 0 To lngCount - 1
                If .AutoAnswerOneRow(mlngCursorY, lngLevel, True) = False Then
                    Exit For
                End If
                If (mlngShowStep >= 2) Then UpdateGameScreen 0, mlngCursorY, -1, 1
                If (mlngShowStep >= 3) Then UpdateSmallView
                DoEvents
            Next mlngCursorY
            
            lngCur = .RestSquares
            If (lngCur < lngPrev) Then Exit For
            DoEvents
        Next lngLevel
    End With
    
    EnableAutoAnswerMenu True
    mlngCursorX = lngSaveX
    mlngCursorY = lngSaveY
    UpdateGameScreen 0, 0, -1, -1
    UpdateSmallView
    
    MsgBox "完了"
End Sub

Private Sub mnuAnswerShowStep_Click(Index As Integer)
'------------------------------------------------------------------------------
'「自動解答」−「ステップ表示」
'------------------------------------------------------------------------------
Dim i As Long

    mlngShowStep = Index
    For i = mnuAnswerShowStep.LBound To mnuAnswerShowStep.UBound
        mnuAnswerShowStep(i).Checked = False
    Next i
    mnuAnswerShowStep(mlngShowStep).Checked = True
End Sub

Private Sub mnuGameExit_Click()
'------------------------------------------------------------------------------
'「ゲーム」−「終了」
'------------------------------------------------------------------------------
    Unload Me
End Sub

Private Sub mnuGameLoad_Click()
'------------------------------------------------------------------------------
'「ゲーム」−「局面をロード」
'ファイルから局面をロードする
'------------------------------------------------------------------------------
Dim strFileName As String

    With cdlFile
        .CancelError = True
        .DefaultExt = "psv"
        If (mlngLastUseDialog <> LASTUSEDLGSTATE) Then .FileName = ""
        .Filter = "Picross Save Data (*.psv)|*.psv"
        .FilterIndex = 1
        .Flags = cdlOFNHideReadOnly Or cdlOFNOverwritePrompt
        .InitDir = gstrAppPath
        mlngLastUseDialog = LASTUSEDLGSTATE
        
        On Error Resume Next
            .ShowOpen
            If Err.Number Then Exit Sub
        On Error GoTo 0
        strFileName = .FileName
    End With
    
    If LoadGameStatus(mobjGame, strFileName, mlngCursorX, mlngCursorY) = False Then
        MsgBox "局面データをロードできませんでした。"
        SetGameRunningFlag False
    Else
        SetGameRunningFlag True
    End If
    ChangeSize
    UpdateGameScreen 0, 0, -1, -1
    UpdateSmallView
End Sub

Private Sub mnuGameOpen_Click()
'------------------------------------------------------------------------------
'「ゲーム」−「問題を開く」
'問題データを開く
'------------------------------------------------------------------------------
Dim strFileName As String

    With cdlFile
        .CancelError = True
        .DefaultExt = "pgd"
        If (mlngLastUseDialog <> LASTUSEDLGGAME) Then .FileName = ""
        .Filter = "Picross Game Data (*.pgd)|*.pgd|Text Files (*.txt;*.ini)|*.txt;*.ini|All Files(*.*)|*.*"
        .FilterIndex = 1
        .Flags = cdlOFNHideReadOnly
        .InitDir = gstrAppPath
        mlngLastUseDialog = LASTUSEDLGGAME
        
        On Error Resume Next
            .ShowOpen
            If Err.Number Then Exit Sub
        On Error GoTo 0
        strFileName = .FileName
    End With
    
    ProcessOpenGame strFileName
End Sub

Private Sub mnuGameSave_Click()
'------------------------------------------------------------------------------
'「ゲーム」−「局面をセーブ」
'現在の局面をファイルにセーブする
'------------------------------------------------------------------------------
Dim strFileName As String

    With cdlFile
        .CancelError = True
        .DefaultExt = "psv"
        If (mlngLastUseDialog <> LASTUSEDLGSTATE) Then .FileName = ""
        .Filter = "Picross Save Data (*.psv)|*.psv"
        .FilterIndex = 1
        .Flags = cdlOFNHideReadOnly Or cdlOFNOverwritePrompt
        .InitDir = gstrAppPath
        mlngLastUseDialog = LASTUSEDLGSTATE
        
        On Error GoTo LabelErrorSave
        .ShowSave
        If Err.Number Then Exit Sub
        strFileName = .FileName
    End With
    
    If SaveGameStatus(mobjGame, strFileName, mlngCursorX, mlngCursorY) = False Then
        MsgBox "致命的エラー：セーブに失敗しました。再試行するか別のファイルに保存してください。"
    End If
    On Error GoTo 0
    
    Exit Sub
    
LabelErrorSave:
    MsgBox "エラーが発生したか、又はキャンセルされました。"
    On Error GoTo 0
    Exit Sub
End Sub

Private Sub mnuShowCol_Click()
'------------------------------------------------------------------------------
'「表示」−「一列(↓)を折り返し表示」
'------------------------------------------------------------------------------
    mnuShowNormal.Checked = False
    mnuShowRow.Checked = False
    mnuShowCol.Checked = True
    hsbLeftCol.Visible = False
    vsbTopRow.Visible = False
    mlngShowMode = 2
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub mnuShowNormal_Click()
'------------------------------------------------------------------------------
'「表示」−「通常表示」
'------------------------------------------------------------------------------
    mnuShowNormal.Checked = True
    mnuShowRow.Checked = False
    mnuShowCol.Checked = False
    hsbLeftCol.Visible = True
    vsbTopRow.Visible = True
    mlngShowMode = 0
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub mnuShowRow_Click()
'------------------------------------------------------------------------------
'「表示」−「一行（→)を折り返し表示」
'------------------------------------------------------------------------------
    mnuShowNormal.Checked = False
    mnuShowRow.Checked = True
    mnuShowCol.Checked = False
    hsbLeftCol.Visible = False
    vsbTopRow.Visible = False
    mlngShowMode = 1
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub mnuTestCont_Click(Index As Integer)
'------------------------------------------------------------------------------
'「テストモード」−「黒と仮定して続行」
'「テストモード」−「白(×)と仮定して続行」
'------------------------------------------------------------------------------

    Select Case Index
        Case 0:     '黒と仮定して続行
            AutoAnswerHairihou mlngCursorX, mlngCursorY, 1, 1
        Case 1:     '白と仮定して続行
            AutoAnswerHairihou mlngCursorX, mlngCursorY, SQUARE_CHECK, 1
    End Select
    
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub mnuTestExit_Click()
'------------------------------------------------------------------------------
'「テストモード」−「現在のレベルを終了」
'------------------------------------------------------------------------------
Dim lngLevel As Long

    lngLevel = mobjGame.ExitTestMode(False, mlngCursorX, mlngCursorY)
    EnableTestModeMenu
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub mnuTestSetAndExitAll_Click()
'------------------------------------------------------------------------------
'「テストモード」−「内容を設定して全レベルを終了」
'------------------------------------------------------------------------------
Dim i As Long

    With mobjGame
        i = .TestModeLevel
        Do While i > 0
            i = .ExitTestMode(True, mlngCursorX, mlngCursorY)
        Loop
    End With
    
    EnableTestModeMenu
    UpdateGameScreen 0, 0, -1, -1
End Sub

Private Sub mnuTestStart_Click()
'------------------------------------------------------------------------------
'「テストモード」−「新しいレベルを開始」
'------------------------------------------------------------------------------
Dim lngLevel As Long

    lngLevel = mobjGame.EnterTestMode(mlngCursorX, mlngCursorY)
    EnableTestModeMenu
    UpdateGameScreen 0, 0, 0, 0
End Sub

Private Sub vsbTopRow_Change()
    UpdateGameScreen 0, 0, 0, 0
End Sub
