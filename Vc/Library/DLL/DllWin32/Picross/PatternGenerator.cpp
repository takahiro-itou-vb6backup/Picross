//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		PatternGenerator.cpp												//
//		Implementation of PatternGenerator class.							//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"
#include "PatternGenerator.h"

//============================================================================
//
//	PatternGenerator class.
//

//////////////////////////////////////////////////////////////////////////////
//	Constructor, Destructor.
PatternGenerator::PatternGenerator(int nLength, LpcPicrossHintNumbers lpcHintNumbers)
{
	m_nLineLength	= nLength;
	memcpy( &m_hintNumbers, lpcHintNumbers, sizeof(PicrossHintNumbers) );
	memset( &m_fixedPattern, 0, sizeof(PicrossLineStatus) );
	memset( &m_nBlockLeftPos, 0, sizeof(m_nBlockLeftPos) );
}

//////////////////////////////////////////////////////////////////////////////
//	Make first/last pattern.

int PatternGenerator::makeFirstPattern(LptPicrossLineStatus lpLine)
{
	int		iHintCount	= ( m_hintNumbers.nCount );
	int		iResult;

	resetSquares();
	iResult		= putBlocksOnLeft( 0, iHintCount - 1, 0 );
	if ( iResult <= 0 ) {
		return ( 0 );
	}
	fillSquares( 0, iHintCount - 1 );

	int		iCheck		= isPatternMatched();
	while ( iCheck <= 0 ) {
		int		iResult		= shiftBlockRight();
		if ( iResult <= 0 ) {
			//	There is no legal patterns !
			return ( 0 );
		}
		iCheck		= isPatternMatched();
		if ( g_lpfnCallbackProgress != NULL ) {
			(*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_CREATE, &m_currentPattern, NULL, -1, -1 );
		}
	}

	memcpy( lpLine, &m_currentPattern, sizeof(PicrossLineStatus) );
	return ( 1 );
}

int PatternGenerator::makeLastPattern(LptPicrossLineStatus lpLine)
{
	int		iHintCount	= ( m_hintNumbers.nCount );

	resetSquares();
	putBlocksOnRight( 0, iHintCount - 1, m_nLineLength - 1 );
	fillSquares( 0, iHintCount - 1 );

	int		iCheck		= isPatternMatched();
	while ( iCheck <= 0 ) {
		int		iResult		= shiftBlockLeft();
		if ( iResult <= 0 ) {
			//	There is no legal patterns !
			return ( 0 );
		}
		iCheck		= isPatternMatched();
	}

	memcpy( lpLine, &m_currentPattern, sizeof(PicrossLineStatus) );
	return ( 1 );
}

int PatternGenerator::findLegalPattern(int nMaxCount)
{
	int		iHintCount	= ( m_hintNumbers.nCount );
	int		iResult;

	resetSquares();
	iResult		= putBlocksOnLeft( 0, iHintCount - 1, 0 );
	if ( iResult <= 0 ) {
		return ( 0 );
	}
	fillSquares( 0, iHintCount - 1 );

	int		iCheck		= isPatternMatched();
	int		i			= 0;

	while ( iCheck <= 0 ) {
		int		iResult		= shiftBlockRight();
		if ( iResult <= 0 ) {
			//	There is no legal patterns !
			return ( 0 );
		}
		iCheck		= isPatternMatched();
		i ++;
		if ( i > nMaxCount ) {
			return ( -1 );
		}
	}

	return ( 1 );
}

//////////////////////////////////////////////////////////////////////////////
//	Make next/previous pattern.
int PatternGenerator::makeNextPattern(LptPicrossLineStatus lpLine)
{
	int		iCheck	= 0;
	while ( iCheck <= 0 ) {
		int		iResult		= shiftBlockRight();
		if ( iResult <= 0 ) {
			//	There are no more legal patterns.
			return ( 0 );
		}
		iCheck		= isPatternMatched();
	}

	memcpy( lpLine, &m_currentPattern, sizeof(PicrossLineStatus) );
	return ( 1 );
}

int PatternGenerator::makePrevPattern(LptPicrossLineStatus lpLine)
{
	int		iCheck	= 0;
	while ( iCheck <= 0 ) {
		int		iResult		= shiftBlockLeft();
		if ( iResult <= 0 ) {
			//	There are no more legal patterns.
			return ( 0 );
		}
		iCheck		= isPatternMatched();
	}

	memcpy( lpLine, &m_currentPattern, sizeof(PicrossLineStatus) );
	return ( 1 );
}

int PatternGenerator::makePrevPattern(LptPicrossLineStatus lpLine, int nBlockIndex)
{
	int		iCheck	= 0;
	while ( iCheck <= 0 ) {
		int		iResult		= shiftBlockLeft( nBlockIndex );
		if ( iResult <= 0 ) {
			return ( 0 );
		}
		iCheck		= isPatternMatched();
	}
	memcpy( lpLine, &m_currentPattern, sizeof(PicrossLineStatus) );
	return ( 1 );
}
