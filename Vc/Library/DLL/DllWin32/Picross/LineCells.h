//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		LineCells.h															//
//		Interface of LineCells class.										//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined( PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__LINE_CELLS_H )
#define PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__LINE_CELLS_H

//============================================================================
//
//	LineCells class.
//

class LineCells {
public:
	//	Constructor, Destructor.
	LineCells(int nLength, LpcPicrossHintNumbers lpcHintNumbers);

	//	Setting fixed pattern.
	void	setupFixedPattern(const LineCells &fixedPattern);

protected:
