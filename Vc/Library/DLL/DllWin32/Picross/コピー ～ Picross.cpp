//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		Picross.cpp															//
//		Main routines of the algorithm.										//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"

LPFN_CALLBACK_SHOWPROGRESS g_lpfnCallbackProgress;

Square		gnEmptySquares[MAX_SQUARES_PER_LINE];
Square		gnCheckSquares[MAX_SQUARES_PER_LINE];
Square		gnBlackSquares[MAX_SQUARES_PER_LINE];

//============================================================================
//
//	Get DLL Version.
//
int WINAPI	getPicrossDLLVersion()
{
	//	Setup table.
	memset( gnEmptySquares, SQUARE_BLANK, sizeof(gnEmptySquares) );
	memset( gnCheckSquares, SQUARE_CHECK, sizeof(gnCheckSquares) );
	memset( gnBlackSquares, COLOR_BLACK, sizeof(gnBlackSquares) );

	//	Return DLL Version.
	return ( 0x00010000 );
}

//============================================================================
//
//	Set callback function.
//

int WINAPI	setCallbackShowingProgress(LPFN_CALLBACK_SHOWPROGRESS lpfnCallback)
{
	g_lpfnCallbackProgress	= lpfnCallback;
	return ( 1 );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Take intersection of two patterns.
//	The result is written into the first pattern 'lpBasePattern'.
//

int		takeIntersection(int nLength,
						 LP_PICROSS_LINE_STATUS lpBasePattern,
						 LPC_PICROSS_LINE_STATUS lpcMaskPattern)
{
	int		i, iUnknownCount;
	int		nBaseSquare, nMaskSquare;

	for ( i = 0, iUnknownCount = 0; i < nLength; i++ ) {
		nBaseSquare		= ( lpBasePattern->nSquares[i] );
		nMaskSquare		= ( lpcMaskPattern->nSquares[i] );

		if ( IS_BLANK_SQUARE(nBaseSquare) || IS_BLANK_SQUARE(nMaskSquare) ) {
			lpBasePattern->nSquares[i]		= SQUARE_BLANK;
			iUnknownCount ++;
		} else if ( nBaseSquare != nMaskSquare ) {
			lpBasePattern->nSquares[i]		= SQUARE_BLANK;
			iUnknownCount ++;
		}
	}
	return ( nLength - iUnknownCount );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Show line.
//

void		fprintHintNumbers(FILE *fp,
							  LPC_PICROSS_HINT_NUMBERS lpcInput)
{
	int		i, iHintCount;
	iHintCount		= ( lpcInput->nCount );
	fprintf( fp, "Hint Numbers:" );
	for ( i = 0; i < iHintCount; i++ ) {
		fprintf( fp, " %d", lpcInput->nNumbers[i] );
	}
	fprintf( fp, "\n" );
}

void		fprintLine(FILE *fp,
					   int nLength,
					   LPC_PICROSS_LINE_STATUS lpcLine)
{
	int		x, iSquare;
	for ( x = 0; x < nLength; x++ ) {
		iSquare		= lpcLine->nSquares[x];
		if ( IS_BLANK_SQUARE(iSquare) ) {
			fprintf( fp, " " );
		} else if ( iSquare & SQUARE_CHECK ) {
			fprintf( fp, "-" );
		} else {
			fprintf( fp, "%1x", (iSquare & SQUARE_COLOR) );
		}
	}
	fprintf( fp, "\n" );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Step the line.
//

int WINAPI	stepTheLine(int nLength,
						LPC_PICROSS_HINT_NUMBERS lpcInput,
						LPC_PICROSS_LINE_STATUS lpcInitPattern,
						LP_PICROSS_LINE_STATUS lpResult)
{
	int						iMake, iIntersection, iFirst;
	int						iTestResult, iProgress;
	int						iHeadCount, iTailCount;
	PICROSS_LINE_STATUS		headPattern, tailPattern, savedInit;
	FILE					*fp, *fh, *ft;

	//	Save initial (fixed) pattern.
	memcpy( &savedInit, lpcInitPattern, sizeof(PICROSS_LINE_STATUS) );
	iFirst		= takeIntersection( nLength, &savedInit, lpcInitPattern );

	fp			= fopen( "Log.txt", "wt" );
	fh			= fopen( "LogHead.txt", "wt" );
	ft			= fopen( "LogTail.txt", "wt" );
	fprintHintNumbers( fp, lpcInput );

	//	Make the first legal line pattern.
	iMake		= makeFirstPattern( nLength, lpcInput, lpcInitPattern, &headPattern );
	iHeadCount	= 0;
	if ( iMake <= 0 ) {
		return ( -1 );
	}
	memcpy( lpResult, &headPattern, sizeof(PICROSS_LINE_STATUS) );
	iIntersection	= nLength;
	fprintLine( fp, nLength, &headPattern );
	fprintLine( fh, nLength, &headPattern );
	iProgress		= (*g_lpfnCallbackProgress)( &headPattern, lpResult, 0, iIntersection );

	//	Make the last legal line pattern.
	iMake		= makeLastPattern( nLength, lpcInput, lpcInitPattern, &tailPattern );
	iTailCount	= 0;
	fprintLine( fp, nLength, &tailPattern );
	fprintLine( ft, nLength, &tailPattern );
	if ( iMake <= 0 ) {
		return ( -1 );
	}

	//	Take intersection.
	iIntersection	= takeIntersection( nLength, lpResult, &tailPattern );
	iProgress		= (*g_lpfnCallbackProgress)( &tailPattern, lpResult, 0, iIntersection );

	//	Make the other patterns, and take intersection for each.
	iTestResult		= iFirst;
	for (;;) {
		iProgress	= (*g_lpfnCallbackProgress)( &headPattern, lpResult, iIntersection, iTestResult );
		if ( iProgress == 0 ) {
			return ( -3 );
		}

		iMake	= makeNextLegalPattern( nLength, lpcInput, lpcInitPattern, &headPattern );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iHeadCount	++;
		fprintLine( fh, nLength, &headPattern );
		iIntersection	= takeIntersection( nLength, lpResult, &headPattern );
		if ( iIntersection <= iTestResult ) { break; }

		iMake	= makePrevLegalPattern( nLength, lpcInput, lpcInitPattern, &tailPattern );
		_ASSERTE( iMake > 0 );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iTailCount ++;
		fprintLine( ft, nLength, &tailPattern );
		iIntersection	= takeIntersection( nLength, lpResult, &tailPattern );
		if ( iIntersection <= iTestResult ) { break; }
	}

	fclose( fp );
	fclose( fh );
	fclose( ft );

	return ( 1 );
}

#if 0
int WINAPI	stepTheLine(int nLength,
						LPC_PICROSS_HINT_NUMBERS lpcInput,
						LPC_PICROSS_LINE_STATUS lpcInitPattern,
						LP_PICROSS_LINE_STATUS lpResult,
						int nOption)
{
	int						iMake, iIntersection;
	int						iFirst, iProgress, iTestResult;
	PICROSS_LINE_STATUS		nextPattern, lastPattern, savedInit;

	//	Save initial (fixed) pattern.
	memcpy( &savedInit, lpcInitPattern, sizeof(PICROSS_LINE_STATUS) );
	iFirst		= intersectionPatterns( nLength, &savedInit, lpcInitPattern );

	//	Make the last legal line pattern.
	iMake		= makeLastPattern( nLength, lpcInput, lpcInitPattern, &lastPattern );
	if ( iMake <= 0 ) {
		return ( -1 );
	}
	memcpy( lpResult, &lastPattern, sizeof(PICROSS_LINE_STATUS) );
	iIntersection	= nLength;
	iProgress		= (*g_lpfnCallbackProgress)( &lastPattern, lpResult, iIntersection, iFirst );

	//	Make the first legal line pattern, and take intersection.
	iMake			= makeFirstPattern( nLength, lpcInput, lpcInitPattern, &nextPattern );
	if ( iMake <= 0 ) {
		return ( -1 );
	}
	iIntersection	= intersectionPatterns( nLength, lpResult, &nextPattern );
	iProgress		= (*g_lpfnCallbackProgress)( &nextPattern, lpResult, iIntersection, iFirst );

	if ( iFirst == 0 && (nOption & 1) ) {
		iTestResult		= executeInternalTest( nLength, lpcInput, lpcInitPattern, lpResult );
		iProgress		= (*g_lpfnCallbackProgress)( &nextPattern, lpResult, iIntersection, iFirst );
	}

	//	Make the other patterns, and take intersection for each.
	for (;;) {
		iProgress	= (*g_lpfnCallbackProgress)( &nextPattern, lpResult, iIntersection, iTestResult );
		if ( iProgress == 0 ) {
			return ( -3 );
		}

		iMake	= makeNextLegalPattern( nLength, lpcInput, lpcInitPattern, &nextPattern );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iIntersection	= intersectionPatterns( nLength, lpResult, &nextPattern );
		if ( iIntersection <= iTestResult ) { break; }

		if ( iFirst > 0 ) { continue; }
		iMake	= makePrevLegalPattern( nLength, lpcInput, lpcInitPattern, &lastPattern );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iIntersection	= intersectionPatterns( nLength, lpResult, &lastPattern );
		if ( iIntersection <= iTestResult ) { break; }
	}
	return ( iIntersection );
}
#endif
