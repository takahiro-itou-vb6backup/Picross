//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		PicrossDebug.h														//
//		Interface of debug functions.										//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined( PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PICROSS_DEBUG_H )
#	 define   PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PICROSS_DEBUG_H

//============================================================================
//
//	Prototype declarations.
//

void	fprintHintNumbers(FILE *, LpcPicrossHintNumbers);
void	fprintLine(FILE *fp, int, LpcPicrossLineStatus);

#endif
