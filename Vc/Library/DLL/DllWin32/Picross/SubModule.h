//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		SubModule.h															//
//		Interface of sub module functions.									//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined( PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__SUB_MODULE_H )
#	 define   PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__SUB_MODULE_H

//============================================================================
//
//	Prototype declarations.
//

int		takeIntersection(int, LptPicrossLineStatus, LpcPicrossLineStatus);
int		takeStrictIntersection(int, LpcPicrossLineStatus,
							   LptPicrossLineStatus, LpcPicrossLineStatus);

int		checkSidePatterns(int, LpcPicrossHintNumbers,
						  LptPicrossLineStatus, LptPicrossLineStatus);

int seive1(int nLength,
		   LpcPicrossHintNumbers lpcHintNumbers,
		   LpcPicrossLineStatus lpcFixedPattern,
		   LptPicrossLineStatus lpResult,
		   int nFirst);
int seive2(int nLength,
		   LpcPicrossHintNumbers lpcHintNumbers,
		   LpcPicrossLineStatus lpcFixedPattern,
		   LptPicrossLineStatus lpResult,
		   int nFirst);

#endif
