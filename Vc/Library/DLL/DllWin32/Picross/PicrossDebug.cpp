//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		PicrossDebug.cpp													//
//		Implements of debug functions.										//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"

//////////////////////////////////////////////////////////////////////////////
//
//	Show line.
//

#if defined( _DEBUG )
void	fprintHintNumbers(FILE *fp,
						  LpcPicrossHintNumbers lpcInput)
{
	int		i, iHintCount;
	iHintCount		= ( lpcInput->nCount );
	fprintf( fp, "Hint Numbers:" );
	for ( i = 0; i < iHintCount; i++ ) {
		fprintf( fp, " %d", lpcInput->nNumbers[i] );
	}
}

void	fprintLine(FILE *fp,
				   int nLength,
				   LpcPicrossLineStatus lpcLine)
{
	if ( fp == NULL ) {
		return;
	}

	int		x, iSquare;
	for ( x = 0; x < nLength; x++ ) {
		iSquare		= lpcLine->nSquares[x];
		if ( IS_BLANK_SQUARE(iSquare) ) {
			fprintf( fp, "-" );
		} else if ( iSquare & SQUARE_CHECK ) {
			fprintf( fp, "x" );
		} else {
			fprintf( fp, "%1x", (iSquare & SQUARE_COLOR) );
		}
	}
}
#endif
