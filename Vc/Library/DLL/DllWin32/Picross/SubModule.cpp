//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		SubModule.cpp														//
//		Implements of sub module functions.									//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"
#include "SubModule.h"
#include "PatternGenerator.h"

//////////////////////////////////////////////////////////////////////////////
//
//	Take intersection of two patterns.
//	The result is written into the first pattern 'lpBasePattern'.
//

int		takeIntersection(int nLength,
						 LptPicrossLineStatus lpBasePattern,
						 LpcPicrossLineStatus lpcMaskPattern)
{
	int		i, iUnknownCount;
	int		nBaseSquare, nMaskSquare;

	for ( i = 0, iUnknownCount = 0; i < nLength; i++ ) {
		nBaseSquare		= ( lpBasePattern->nSquares[i] );
		nMaskSquare		= ( lpcMaskPattern->nSquares[i] );

		if ( IS_BLANK_SQUARE(nBaseSquare) || IS_BLANK_SQUARE(nMaskSquare) 
			 || (nBaseSquare != nMaskSquare) )
		{
			lpBasePattern->nSquares[i]		= SQUARE_BLANK;
			lpBasePattern->nBlockIndex[i]	= 0;
			iUnknownCount ++;
		}
	}
	return ( nLength - iUnknownCount );
}

int		takeStrictIntersection(int nLength,
							   LpcPicrossLineStatus lpcFixedPattern,
							   LptPicrossLineStatus lpBasePattern,
							   LpcPicrossLineStatus lpcMaskPattern)
{
	int		i, iUnknownCount;
	int		iFixedSquare, iBaseSquare, iMaskSquare;

	for ( i = 0, iUnknownCount = 0; i < nLength; i++ ) {
		iFixedSquare	= ( lpcFixedPattern->nSquares[i] );
		iBaseSquare		= ( lpBasePattern->nSquares[i] );
		iMaskSquare		= ( lpcMaskPattern->nSquares[i] );

		if ( IS_BLANK_SQUARE(iFixedSquare) ) {
			if ( (lpBasePattern->nBlockIndex[i]) != (lpcMaskPattern->nBlockIndex[i]) ) {
				lpBasePattern->nSquares[i]		= SQUARE_BLANK;
				lpBasePattern->nBlockIndex[i]	= -1;
				iUnknownCount ++;
				continue;
			}
			if ( IS_BLANK_SQUARE(iBaseSquare) || IS_BLANK_SQUARE(iMaskSquare)
				 || (iBaseSquare & SQUARE_CHECK) || (iMaskSquare & SQUARE_CHECK)
				 || (iBaseSquare != iMaskSquare) ) 
			{
				lpBasePattern->nSquares[i]		= SQUARE_BLANK;
				lpBasePattern->nBlockIndex[i]	= 0;
				iUnknownCount ++;
			}
		} else {
			_ASSERTE( iFixedSquare == iBaseSquare && iFixedSquare == iMaskSquare );
			lpBasePattern->nSquares[i]		= iFixedSquare;
		}
	}
	return ( nLength - iUnknownCount );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Check special patterns.
//

int		checkSidePatterns(int nLength,
						  LpcPicrossHintNumbers lpcInput,
						  LptPicrossLineStatus lpFixedPattern,
						  LptPicrossLineStatus lpLine)
{
	int						iMake, iIntersection, iCellCount;
	int						iFixedSquare, iTestSquare;
	int						iHintCount, iFirstLeft, iLastRight;
	PicrossLineStatus		headPattern, tailPattern;
	PatternGenerator		headGenerator( nLength, lpcInput );
	PatternGenerator		tailGenerator( nLength, lpcInput );

	headGenerator.setupFixedPattern( lpFixedPattern );
	tailGenerator.setupFixedPattern( lpFixedPattern );

	iMake		= headGenerator.makeFirstPattern( &headPattern );
	if ( iMake <= 0 ) {
		return ( -1 );
	}

	iMake		= tailGenerator.makeLastPattern( &tailPattern );
	if ( iMake <= 0 ) {
		return ( -1 );
	}

	iHintCount		= ( lpcInput->nCount );
	iFirstLeft		= headGenerator.getBlockLeftPos( 0 );
	iLastRight		= tailGenerator.getBlockLeftPos( iHintCount - 1 );
	iLastRight		+= ( lpcInput->nNumbers[iHintCount-1] );

	memcpy( lpLine, &headPattern, sizeof(PicrossLineStatus) );
	iIntersection	= takeStrictIntersection( nLength, lpFixedPattern, lpLine, &tailPattern );

	iCellCount		= 0;
	for ( int i = 0; i < nLength; i++ ) {
		iFixedSquare		= ( lpFixedPattern->nSquares[i] );
		iTestSquare			= ( lpLine->nSquares[i] );

		if ( (i < iFirstLeft) || (i >= iLastRight) ) {
			_ASSERTE( IS_BLANK_SQUARE(iFixedSquare) || (iFixedSquare == SQUARE_CHECK) );

			iFixedSquare			= SQUARE_CHECK;
			iTestSquare				= SQUARE_CHECK;
		}

		lpLine->nSquares[i]			= iTestSquare;
		lpFixedPattern->nSquares[i]	= iFixedSquare;
		if ( !IS_BLANK_SQUARE(iTestSquare) ) {
			iCellCount ++;
		}
	}
	return ( iCellCount );
}


//////////////////////////////////////////////////////////////////////////////
//
//	Seives for speed up.
//

int seive1(int nLength,
		   LpcPicrossHintNumbers lpcHintNumbers,
		   LpcPicrossLineStatus lpcFixedPattern,
		   LptPicrossLineStatus lpResult,
		   int nFirst)
{
	int						iMake, iIntersection, iProgress;
	PicrossLineStatus		currentPattern;
	PatternGenerator		patternGenerator( nLength, lpcHintNumbers );
	patternGenerator.setupFixedPattern( lpcFixedPattern );

	iMake				= patternGenerator.makeLastPattern( &currentPattern );
	memcpy( lpResult, &currentPattern, sizeof(PicrossLineStatus) );
	iIntersection		= nLength;

	for (;;) {
		iMake	= patternGenerator.makePrevPattern( &currentPattern, -1 );
		if ( iMake == 0 )	{ break; }
		iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );
		if ( g_lpfnCallbackProgress != NULL ) {
			iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_PROCEED,
				&currentPattern, lpResult, iIntersection, nFirst );
		}
		if ( iIntersection <= nFirst ) {
			break;
		}
	}
	return ( iIntersection );
}

int seive2(int nLength,
		   LpcPicrossHintNumbers lpcHintNumbers,
		   LpcPicrossLineStatus lpcFixedPattern,
		   LptPicrossLineStatus lpResult,
		   int nFirst)
{
	int						iMake, iIntersection, iFixed, iSquare, iProgress;
	PicrossLineStatus		testPattern;
	PatternGenerator		testGenerator( nLength, lpcHintNumbers );

	iIntersection		= nFirst;
	memcpy( &testPattern, lpcFixedPattern, sizeof(PicrossLineStatus) );
	for ( int x = 0; x < nLength; x++ ) {
		iFixed		= ( lpcFixedPattern->nSquares[x] );
		if ( !IS_BLANK_SQUARE(iFixed) ) {
			continue;
		}

		iSquare		= ( lpResult->nSquares[x] );
		if ( IS_BLANK_SQUARE(iSquare) ) {
			continue;
		}

		if ( iSquare & SQUARE_CHECK ) {
			testPattern.nSquares[x]		= COLOR_BLACK;
		} else if ( (iSquare & SQUARE_COLOR) != 0 ) {		
			testPattern.nSquares[x]		= SQUARE_CHECK;
		} else {
			continue;
		}

		testGenerator.setupFixedPattern( &testPattern );
		iMake	= testGenerator.findLegalPattern( 1024 );
		_ASSERTE ( iMake >= 0 );

		if ( iMake == 0 ) {
			testPattern.nSquares[x]		= iSquare;
		} else if ( iMake < 0 ) {
			//	Canceled.
			lpResult->nSquares[x]		= SQUARE_BLANK;
			testPattern.nSquares[x]		= SQUARE_BLANK;
			iIntersection	--;
		} else {
			//	There are legal pattern, or canceled.
			lpResult->nSquares[x]		= SQUARE_BLANK;
			testPattern.nSquares[x]		= SQUARE_BLANK;
			iIntersection	--;
		}
		if ( g_lpfnCallbackProgress != NULL ) {
			iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_PROCEED,
				&testPattern, lpResult, iIntersection, -1 );
		}
	}
	return ( iIntersection );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Step the line.
//

#if 0
int WINAPI	stepTheLine(int nLength,
						LpcPicrossHintNumbers lpcInput,
						LpcPicrossLineStatus lpcInitPattern,
						LptPicrossLineStatus lpResult)
{
	int						iMake, iIntersection, iFirst, iSide;
	int						iTestResult, iProgress;
	int						iHeadCount, iFixedCount;
	PicrossLineStatus		currentPattern, savedInit, fixedPattern;

	memcpy( &fixedPattern, lpcInitPattern, sizeof(PicrossLineStatus) );

	//	Save initial (fixed) pattern.
	memcpy( &savedInit, &fixedPattern, sizeof(PicrossLineStatus) );
	iFixedCount	= takeIntersection( nLength, &savedInit, &fixedPattern );

	//	Initialize pattern generator.
	PatternGenerator		patternGenerator( nLength, lpcInput );
	patternGenerator.setupFixedPattern( &fixedPattern );

#if defined( _DEBUG )
	int						iTailCount, iCutOff = 0;
	PicrossLineStatus		tailPattern;
	PatternGenerator		tailGenerator( nLength, lpcInput );
	tailGenerator.setupFixedPattern( &fixedPattern );
#endif

	//	Check special patterns.
	iFirst		= checkSpecialPatterns( nLength, lpcInput, &fixedPattern, lpResult );
	if ( iFirst != 0 ) {
		return ( iFirst );
	}

#if 0
	iSide		= checkSidePatterns( nLength, lpcInput, &fixedPattern, lpResult );
	if ( iSide < 0 ) {
		return ( iSide );
	}
	memcpy( &fixedPattern, lpResult, sizeof(PicrossLineStatus) );
#endif

	//	Determine colored squares.
	iSide		= determineColors1( nLength, lpcInput, &fixedPattern, lpResult, iFixedCount );
	memcpy( &fixedPattern, lpResult, sizeof(PicrossLineStatus) );

	//	Save initial (fixed) pattern.
	memcpy( &savedInit, &fixedPattern, sizeof(PicrossLineStatus) );
	iFirst		= takeIntersection( nLength, &savedInit, &fixedPattern );
	iProgress		= (*g_lpfnCallbackProgress)( &fixedPattern, lpResult, iFirst, iSide );

	_ASSERTE( iFirst == iSide );
	return ( iSide );

#if defined( _DEBUG )
	FILE					*fp, *fh, *ft;
	fp			= fopen( "Log.txt", "wt" );
	fh			= fopen( "LogHead.txt", "wt" );
	ft			= fopen( "LogTail.txt", "wt" );
	if ( fp != NULL ) {
		fprintHintNumbers( fp, lpcInput );
		fprintf( fp, "\n" );

		fprintf( fp, "Given initial pattern, and current fixed cells :\n" );
		fprintLine( fp, nLength, lpcInitPattern );
		fprintf( fp, " : " );
		fprintLine( fp, nLength, &fixedPattern );
		fprintf( fp, "\n\n" );

		fprintf( fp, "Side pattern intersection :\n" );
		fprintLine( fp, nLength, lpResult );
		fprintf( fp, " : Side = %d\n\n", iSide );
	}
#endif

	//	Seive 1.
	iIntersection	= seive1( nLength, lpcInput, &fixedPattern, lpResult, iFirst );
	_ASSERTE( iIntersection >= iFirst );

#if defined( _DEBUG )
	fprintf( fp, "Complete seive 1\n" );
	fprintLine( fp, nLength, lpResult );
	fprintf( fp, "\n\n" );
#endif

	//	Check some last patterns.
	iTestResult		= iSide;
	iMake		= patternGenerator.makeLastPattern( &currentPattern );
	iHeadCount	= 0;
	if ( iMake <= 0 ) { return ( -1 ); }
	iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );

	for ( iHeadCount = 0; iHeadCount < nLength; iHeadCount++ ) {
		iProgress	= (*g_lpfnCallbackProgress)( &currentPattern, lpResult, iIntersection, iTestResult );
		if ( iProgress == 0 )	{ return ( -3 ); }

		iMake	= patternGenerator.makePrevPattern( &currentPattern );
		if ( iMake == 0 )		{ break; }
		if ( iMake < 0 )		{ return ( iMake ); }

		iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );
		if ( iIntersection <= iTestResult ) { break; }
	}

#if defined( _DEBUG )
	fprintf( fp, "Complete seive 2\n" );
	fprintLine( fp, nLength, lpResult );
	fprintf( fp, "\n\n" );
#endif

	//	Make the first legal line pattern.
	iMake		= patternGenerator.makeFirstPattern( &currentPattern );
	iHeadCount	= 0;
	if ( iMake <= 0 ) { return ( -1 ); }
	iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );

#if defined( _DEBUG )
	if ( fp != NULL ) {
		fprintLine( fp, nLength, &currentPattern );	fprintf( fp, " : " );
		fprintLine( fp, nLength, lpResult );		fprintf( fp, "\n" );
	}
	if ( fh != NULL ) {
		fprintLine( fh, nLength, &currentPattern );	fprintf( fh, " : " );
		fprintLine( fh, nLength, lpResult );		fprintf( fh, "\n" );
	}
#endif
	iProgress		= (*g_lpfnCallbackProgress)( &currentPattern, lpResult, 0, iIntersection );

#if defined( _DEBUG )
	//	Make the last legal line pattern.
	iMake		= tailGenerator.makeLastPattern( &tailPattern );
	iTailCount	= 0;
	if ( iMake <= 0 ) {
		return ( -1 );
	}

	//	Take intersection with strict block index.
	iIntersection	= takeIntersection( nLength, lpResult, &tailPattern );
	iProgress		= (*g_lpfnCallbackProgress)( &tailPattern, lpResult, 0, iIntersection );
	if ( fp != NULL ) {
		fprintLine( fp, nLength, &tailPattern );	fprintf( fp, " : " );
		fprintLine( fp, nLength, lpResult );		fprintf( fp, "\n" );
	}
	if ( ft != NULL ) {
		fprintLine( ft, nLength, &tailPattern );	fprintf( ft, " : " );
		fprintLine( ft, nLength, lpResult );		fprintf( ft, "\n" );
	}
#endif

	//	Make the other patterns, and take intersection for each.
	for (;;) {
		iProgress	= (*g_lpfnCallbackProgress)( &currentPattern, lpResult, iIntersection, iTestResult );
		if ( iProgress == 0 ) {
			return ( -3 );
		}

		iMake	= patternGenerator.makeNextPattern( &currentPattern );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iHeadCount	++;

		iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );
#if defined( _DEBUG )
		fprintLine( fp, nLength, &currentPattern );	fprintf( fp, " : " );
		fprintLine( fh, nLength, &currentPattern );	fprintf( fh, " : " );
		fprintLine( fp, nLength, lpResult );		fprintf( fp, "\n" );
		fprintLine( fh, nLength, lpResult );		fprintf( fh, "\n" );
		if ( (iIntersection <= iTestResult) && (iCutOff == 0) ) {
			fprintf( fp, "Cut off\n" );
			iCutOff			= 1;
			break;
		}

		iMake	= tailGenerator.makePrevPattern( &tailPattern );
		_ASSERTE( iMake > 0 );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iTailCount ++;

		fprintLine( ft, nLength, &tailPattern );	fprintf( ft, " : " );
		fprintLine( ft, nLength, lpResult );		fprintf( ft, "\n" );
#else
		if ( iIntersection <= iTestResult ) {
			break;
		}
#endif
	}

#if defined( _DEBUG )
	fclose( fp );
	fclose( fh );
	fclose( ft );
#endif

	return ( iIntersection );
}
#endif
