//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		PatternGenerator.h													//
//		Interface of PatternGenerator class.								//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined( PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PATTERN_GENERATOR_H )
#	 define   PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PATTERN_GENERATOR_H

//============================================================================
//
//	Errors.
//

#define GENERATE_ERROR_OUT_OF_LINE			(-2)
#define GENERATE_ERROR_BLANK_SQUARE			(-3)
#define GENERATE_ERROR_CHECK_IN_FIXED		(-4)
#define GENERATE_ERROR_COLOR_IN_FIXED		(-5)
#define GENERATE_ERROR_COLOR_MISMATCH		(-6)
#define GENERATE_ERROR_OVER_CONTINUED		(-7)
#define GENERATE_ERROR_CANNOT_FILL_COLOR	(-8)
#define GENERATE_ERROR_CANNOT_PUT_BLOCKS	(-9)

//============================================================================
//
//	Macros for generating pattern.
//

//	isLeftSameColor/isRightSameColor(pos,color)
//	checks if the square (specified by pos,) has same color with pos.
#define isLeftSameColor(pos, color)		\
	( ((pos) >= 0) && (m_fixedPattern.nSquares[(pos)] == (color)) )
#define isRightSameColor(pos, color)	\
	( ((pos) < m_nLineLength) && (m_fixedPattern.nSquares[(pos)] == (color)) )

//	getHintNumber/getHintColor
//	gets number/color of specified block.

//	checkDefaultColor macro checks color number is 0 or not.
//	and if color number is 0, change color to default (BLACK).

#define getHintNumber(block)		( m_hintNumbers.nNumbers[(block)] )
#define getHintColor(block)			( m_hintNumbers.nColors[(block)] )
#define checkDefaultColor(color)	if ( color == 0 ) { color = COLOR_BLACK; }

#define setBlockPos(block, pos)		m_nBlockLeftPos[ (block) ]	= ( pos )
#define getBlockPos(block)			( m_nBlockLeftPos[(block)] )

//============================================================================
//
//	PatternGenerator class.
//

class PatternGenerator {
public:
	//	Constructor, Destructor.
	PatternGenerator(int nLength, LpcPicrossHintNumbers lpcHintNumbers);

	//	Setting fixed pattern;
	void	setupFixedPattern(LpcPicrossLineStatus lpcFixed);

	//	Reset square pattern. Copy from fixed pattern.
	void	resetSquares();

	//	Fill color block, check marks.
	int		fillSquares(int nFirstBlock, int nLastBlock);
	int		fillColor(int nBlockIndex);
	int		fillCheck(int nStart, int nCount);

	//	Get current pattern.
	void	getCurrentPattern(LptPicrossLineStatus lpDest);

	//	Get block position
	int		getBlockLeftPos(int nBlockIndex);

public:
	//	Put block as left/right as possible.
	int		putBlockOnLeft(int nBlockIndex, int & rfLeft, int nTestWhite);
	int		putBlockOnRight(int nBlockIndex, int & rfRight, int nTestWhite);

	//	Put specified range block(s) as left/right as possible.
	int		putBlocksOnLeft(int nFirstBlock, int nLastBlock, int nLeft);
	int		putBlocksOnRight(int nFirstBlock, int nLastBlock, int nRight);

public:
	//	Make first/last pattern.
	int		makeFirstPattern(LptPicrossLineStatus lpLine);
	int		makeLastPattern(LptPicrossLineStatus lpLine);
	int		findLegalPattern(int nMaxCount);

	//	Make next/previous pattern.
	int		makeNextPattern(LptPicrossLineStatus lpLine);
	int		makePrevPattern(LptPicrossLineStatus lpLine);
	int		makePrevPattern(LptPicrossLineStatus lpline, int nBlockIndex);

protected:
	//	Check pattern if the pattern matches the fixed-pattern.
	int		isPatternMatched() const;

	//	Shift a block to right/left
	//	(and arrange rest blocks as left/right as possible).
	int		shiftBlockRight();
	int		shiftBlockLeft();
	int		shiftBlockLeft(int nBlockIndex);

private:
	int						m_nLineLength;
	int						m_nBlockLeftPos[MAX_HINTS_PER_LINE];
	PicrossHintNumbers		m_hintNumbers;
	PicrossLineStatus		m_currentPattern;
	PicrossLineStatus		m_fixedPattern;
};

//============================================================================
//
//	Inline member functions of PatternGenerator class.
//

//////////////////////////////////////////////////////////////////////////////
//	setupFixedPattern.
//	Set member m_fixedPattern which hold initial square pattern.

inline void PatternGenerator::setupFixedPattern(LpcPicrossLineStatus lpcFixed)
{
	memcpy( &m_fixedPattern, lpcFixed, sizeof(PicrossLineStatus) );
}

inline int PatternGenerator::getBlockLeftPos(int nBlockIndex)
{
	if ( (nBlockIndex < 0) || (nBlockIndex >= (m_hintNumbers.nCount)) ) {
		return ( -1 );
	}
	return ( m_nBlockLeftPos[nBlockIndex] );
}

inline void PatternGenerator::getCurrentPattern(LptPicrossLineStatus lpDest)
{
	memcpy( lpDest, &m_currentPattern, sizeof(PicrossLineStatus) );
}

//////////////////////////////////////////////////////////////////////////////
//	Check pattern if the pattern matches the fixed-pattern.
inline int PatternGenerator::isPatternMatched() const
{
	for ( int i = 0; i < m_nLineLength; i++ ) {
		int		iFixedSquare	= ( m_fixedPattern.nSquares[i] );
		int		iFixedColor		= ( iFixedSquare & SQUARE_COLOR );
		int		iPatternSquare	= ( m_currentPattern.nSquares[i] );

		if ( iFixedSquare & SQUARE_CHECK ) {
			if ( (iPatternSquare & SQUARE_CHECK) == 0 ) {
				return ( GENERATE_ERROR_CHECK_IN_FIXED );
			}

		} else if ( iFixedColor != 0 ) {
			if ( IS_BLANK_SQUARE(iPatternSquare) ) {
				return ( GENERATE_ERROR_BLANK_SQUARE );
			}
			if ( iPatternSquare & SQUARE_CHECK ) {
				return ( GENERATE_ERROR_COLOR_IN_FIXED );
			}
			if ( (iPatternSquare & SQUARE_COLOR) != iFixedColor ) {
				return ( GENERATE_ERROR_COLOR_MISMATCH );
			}
		}
	}
	return ( 1 );
}

//////////////////////////////////////////////////////////////////////////////
//	Fill color block, check marks.

inline void PatternGenerator::resetSquares()
{
	memcpy( &m_currentPattern, &m_fixedPattern, sizeof(PicrossLineStatus) );
}

inline int PatternGenerator::fillSquares(int nFirstBlock, int nLastBlock)
{
	int		iHintCount		= ( m_hintNumbers.nCount );
	int		iStart			= 0;

	if ( nLastBlock < 0 ) {
		nLastBlock		= iHintCount - 1;
	}

	for ( int iBlock = 0; iBlock <= nLastBlock; iBlock++ ) {
		int		iLeft			= getBlockPos ( iBlock );

		if ( iLeft < iStart ) {
			return ( -iBlock );
		}
		fillCheck( iStart, iLeft - iStart );
		int		iHintNumber		= fillColor( iBlock );
		if ( iHintNumber <= 0 ) {	//	This means error.
			return ( -iBlock );
		}
		iStart		= iLeft + iHintNumber;
	}
	fillCheck( iStart, m_nLineLength - iStart );
	return ( 1 );
}

inline int PatternGenerator::fillColor(int nBlockIndex)
{
	int		i;
	int		iHintNumber		= getHintNumber ( nBlockIndex );
	int		iColor			= getHintColor ( nBlockIndex );
	int		iStart			= getBlockPos ( nBlockIndex );

	if ( (iStart < 0) || (iStart + iHintNumber > m_nLineLength) ) {
		return ( GENERATE_ERROR_OUT_OF_LINE );
	}
	for ( i = 0; i < iHintNumber; i++ ) {
		m_currentPattern.nSquares[iStart + i]	= iColor;
		m_currentPattern.nBlockIndex[iStart+i]	= nBlockIndex;
	}
	return ( iHintNumber );
}

inline int PatternGenerator::fillCheck(int nStart, int nCount)
{
	if ( (nStart < 0) || (nStart + nCount > m_nLineLength) ) {
		return ( GENERATE_ERROR_OUT_OF_LINE );
	}
	if ( nCount <= 0 ) {
		return ( nCount );
	}
	for ( int i = 0; i < nCount; i++ ) {
		m_currentPattern.nSquares[nStart+i]		= SQUARE_CHECK;
		m_currentPattern.nBlockIndex[nStart+i]	= -1;
	}
	return ( nCount );
}

//////////////////////////////////////////////////////////////////////////////
//	put block as left/right as possible.

inline int PatternGenerator::putBlockOnLeft(int nBlockIndex, int & rfLeft, int nTestWhite)
{
	int		i, iSquare, iColor, iPrevRight;
	int		iHintNumber		= getHintNumber( nBlockIndex );
	int		x				= rfLeft;
	int		iRight			= m_nLineLength - iHintNumber;
	int		iLegal			= 0;
	int		iHintColor		= getHintColor( nBlockIndex );
	checkDefaultColor( iHintColor );

	while ( (iLegal == 0) && (x <= iRight) ) {
		setBlockPos( nBlockIndex, x );
		iLegal		= 1;
		if ( isLeftSameColor(x-1, iHintColor)
			 || isRightSameColor(x+iHintNumber, iHintColor) )
		{
			x ++;
			iLegal		= 0;
			continue;
		}
		for ( i = 0; i < iHintNumber; i++ ) {
			iSquare		= m_fixedPattern.nSquares[ x+i ];
			iColor		= ( iSquare & SQUARE_COLOR );
			if ( iSquare & SQUARE_CHECK ) {
				x			= x + i + 1;
				iLegal		= 0;
				break;
			} else if ( (iColor != 0) && (iColor != iHintColor) ) {
				return ( GENERATE_ERROR_COLOR_MISMATCH );
			}
		}
	}
	if ( iLegal == 0 ) {
		return ( GENERATE_ERROR_CANNOT_FILL_COLOR );
	}

	//	Test white square.
	if ( (nTestWhite & 1) && (nBlockIndex > 0) ) {
		iHintNumber		= getHintNumber ( nBlockIndex - 1 );
		iPrevRight		= getBlockPos ( nBlockIndex - 1 ) + iHintNumber;
		//	The squares where nPrevRight..x-1, fill in white.
		//	Check there are no colord squares in fixed pattern.
		for ( i = iPrevRight; i < x; i++ ) {
			iSquare		= m_fixedPattern.nSquares[ i ];
			if ( IS_BLANK_SQUARE(iSquare) || (iSquare & SQUARE_CHECK) ) {
				continue;
			}
			rfLeft		= i - iHintNumber;
			return ( GENERATE_ERROR_COLOR_MISMATCH );
		}
	}

	//	Return the left side position.
	return ( x );
}

inline int PatternGenerator::putBlockOnRight(int nBlockIndex, int & rfRight, int nTestWhite)
{
	int		i, iSquare, iColor, iPrevLeft, iBlockRight;
	int		iHintCount		= m_hintNumbers.nCount;
	int		iHintNumber		= getHintNumber( nBlockIndex );
	int		x				= rfRight - iHintNumber + 1;
	int		iLegal			= 0;
	int		iHintColor		= getHintColor( nBlockIndex );
	checkDefaultColor( iHintColor );

	while ( (iLegal == 0) && (x >= 0) ) {
		setBlockPos ( nBlockIndex, x );
		iLegal		= 1;
		if ( isLeftSameColor(x-1, iHintColor)
			 || isLeftSameColor(x+iHintNumber, iHintColor) )
		{
			x --;
			iLegal		= 0;
			continue;
		}
		for ( int i = 0; i < iHintNumber; i++ ) {
			iSquare		= m_fixedPattern.nSquares[ x+i ];
			iColor		= ( iSquare & SQUARE_COLOR );
			if ( iSquare & SQUARE_CHECK ) {
				x			= x + i - iHintNumber;
				iLegal		= 0;
			} else if ( (iColor != 0) && (iColor != iHintColor) ) {
				return ( GENERATE_ERROR_COLOR_MISMATCH );
			}
		}
	}
	if ( iLegal == 0 ) {
		return ( GENERATE_ERROR_CANNOT_FILL_COLOR );
	}

	//	Test white square.
	if ( (nTestWhite & 1) && (nBlockIndex < iHintCount - 1) ) {
		iBlockRight		= x + iHintNumber;
		iHintNumber		= getHintNumber ( nBlockIndex + 1 );
		iPrevLeft		= getBlockPos ( nBlockIndex + 1 );
		//	The squares where nPrevRight..x-1, fill in white.
		//	Check there are no colord squares in fixed pattern.
		for ( i = iPrevLeft - 1; i >= iBlockRight; i-- ) {
			iSquare		= ( m_fixedPattern.nSquares[i] );
			if ( IS_BLANK_SQUARE(iSquare) || (iSquare & SQUARE_CHECK) ) {
				continue;
			}
			rfRight		= i + iHintNumber;
			return ( GENERATE_ERROR_COLOR_MISMATCH );
		}
	}

	//	Return the left side position.
	return ( x );
}

inline int PatternGenerator::putBlocksOnLeft(int nFirstBlock, int nLastBlock, int nLeft)
{
	int		iHintNumber, iHintColor;
	int		iFixed, iCheckFlag;
	int		x				= nLeft;
	int		iPrevColor		= -1;

	for ( int iBlockIndex = nFirstBlock; iBlockIndex <= nLastBlock; iBlockIndex++ ) {
		iHintColor			= getHintColor( iBlockIndex );
		checkDefaultColor( iHintColor );
		if ( iHintColor == iPrevColor ) { x ++; }

		int		iResult		= putBlockOnLeft( iBlockIndex, x, 1 );
		if ( iResult == GENERATE_ERROR_COLOR_MISMATCH ) {
			if ( iBlockIndex <= nFirstBlock ) {
				return ( -1 );
			}
			iPrevColor	= -1;
			iBlockIndex	--;
//			x			= getBlockPos ( iBlockIndex ) + 1;
			iBlockIndex	--;
			continue;
		}
		if ( iResult < 0 ) {
			return ( -iBlockIndex );
		}
		iHintNumber			= getHintNumber ( iBlockIndex );

		x			= iResult + iHintNumber;

		//	Check right white squares.
		if ( iBlockIndex == nLastBlock ) {
			iCheckFlag	= 1;
			for ( x = m_nLineLength - 1; x >= iResult + iHintNumber; x-- ) {
				iFixed		= ( m_fixedPattern.nSquares[x] );
				if ( IS_BLANK_SQUARE(iFixed) || (iFixed & SQUARE_CHECK) ) {
					continue;
				}
				iCheckFlag	= 0;
				break;
			}
			if ( iCheckFlag == 0 ) {
				x				= x - iHintNumber + 1;
				iPrevColor		= -1;
				iBlockIndex		--;
				continue;
			}
		}
		iPrevColor	= iHintColor;
	}
	return ( 1 );
}

inline int PatternGenerator::putBlocksOnRight(int nFirstBlock, int nLastBlock, int nRight)
{
	int		iHintNumber, iHintColor;
	int		iFixed, iCheckFlag;
	int		x			= nRight;
	int		iPrevColor		= -1;

	for ( int iBlockIndex = nLastBlock; iBlockIndex >= nFirstBlock; iBlockIndex-- ) {
		iHintColor			= getHintColor( iBlockIndex );
		checkDefaultColor( iHintColor );
		if ( iHintColor == iPrevColor ) { x --; }

		int		iResult		= putBlockOnRight( iBlockIndex, x, 1 );
		if ( iResult == GENERATE_ERROR_COLOR_MISMATCH ) {
			if ( iBlockIndex >= nLastBlock ) {
				return ( -1 );
			}
			iPrevColor	= -1;
			iBlockIndex ++;
			iHintNumber	= getHintNumber( iBlockIndex );
//			x			= getBlockPos ( iBlockIndex ) + iHintNumber - 2;
			iBlockIndex ++;
			continue;
		}
		if ( iResult < 0 ) {
			return ( -iBlockIndex );
		}
		iHintNumber	= getHintNumber( iBlockIndex );

		x			= iResult - 1;
		if ( iBlockIndex == nFirstBlock ) {
			iCheckFlag	= 1;
			for ( x = 0; x < iResult; x++ ) {
				iFixed		= ( m_fixedPattern.nSquares[x] );
				if ( IS_BLANK_SQUARE(iFixed) || (iFixed & SQUARE_CHECK) ) {
					continue;
				}
				iCheckFlag	= 0;
				break;
			}
			if ( iCheckFlag == 0 ) {
				x				= x + iHintNumber - 1;
				iPrevColor		= -1;
				iBlockIndex		++;
				continue;
			}
		}
		iPrevColor	= iHintColor;
	}
	return ( 1 );
}

//////////////////////////////////////////////////////////////////////////////
//	Shift a block to right/left
//	(and arrange rest blocks as left/right as possible).
inline int PatternGenerator::shiftBlockRight()
{
	int		iHintCount		= ( m_hintNumbers.nCount );

	for ( int iBlock = iHintCount - 1; iBlock >= 0; iBlock-- ) {
		resetSquares();

		int		nLeft		= getBlockPos ( iBlock ) + 1;
		int		iPutRests	= putBlocksOnLeft( iBlock, iHintCount - 1, nLeft );
		if ( iPutRests > 0 ) {
			fillSquares( 0, iHintCount - 1 );
			return ( 1 );
		}
	}
	return ( 0 );
}

inline int PatternGenerator::shiftBlockLeft()
{
	int		iHintCount		= ( m_hintNumbers.nCount );

	for ( int iBlock = 0; iBlock < iHintCount; iBlock++ ) {
		resetSquares();

		int		iNumber		= getHintNumber ( iBlock );
		int		iCurRight	= getBlockPos ( iBlock ) + iNumber - 1;
		int		iPutRests	= putBlocksOnRight( 0, iBlock, iCurRight - 1 );
		if ( iPutRests > 0 ) {
			fillSquares( 0, iHintCount - 1 );
			return ( 1 );
		}
	}
	return ( 0 );
}

inline int PatternGenerator::shiftBlockLeft(int nBlockIndex)
{
	int		iHintCount		= ( m_hintNumbers.nCount );
	
	if ( nBlockIndex < 0 ) {
		nBlockIndex			+= iHintCount;
	}
	for ( int iBlock = nBlockIndex; iBlock < iHintCount; iBlock++ ) {
		resetSquares();
		int		iNumber		= getHintNumber ( iBlock );
		int		iCurRight	= getBlockPos ( iBlock ) + iNumber - 1;
		int		iPutRests	= putBlocksOnRight( 0, iBlock, iCurRight - 1 );
		if ( iPutRests > 0 ) {
			fillSquares( 0, iHintCount - 1 );
			return ( 1 );
		}
	}
	return ( 0 );
}

#endif
