//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		Picross.cpp															//
//		Main routines of the algorithm.										//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"
#include "PatternGenerator.h"
#include "SubModule.h"

LPFN_CALLBACK_SHOWPROGRESS g_lpfnCallbackProgress;
int			g_nLevel;

Square		gnEmptySquares[MAX_SQUARES_PER_LINE];
Square		gnCheckSquares[MAX_SQUARES_PER_LINE];
Square		gnBlackSquares[MAX_SQUARES_PER_LINE];

//============================================================================
//
//	Get DLL Version.
//
int WINAPI	getPicrossDLLVersion()
{
	//	Setup table.
	memset( gnEmptySquares, SQUARE_BLANK, sizeof(gnEmptySquares) );
	memset( gnCheckSquares, SQUARE_CHECK, sizeof(gnCheckSquares) );
	memset( gnBlackSquares, COLOR_BLACK, sizeof(gnBlackSquares) );

	//	Return DLL Version.
	return ( 0x00010000 );
}

//============================================================================
//
//	Set callback function.
//

int WINAPI	setCallbackShowingProgress(LPFN_CALLBACK_SHOWPROGRESS lpfnCallback)
{
	g_lpfnCallbackProgress	= lpfnCallback;
	return ( 1 );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Check special patterns.
//

int		checkSpecialPatterns(int nLength,
							 LpcPicrossHintNumbers lpcInput,
							 LpcPicrossLineStatus lpcInitPattern,
							 LptPicrossLineStatus lpOutput)
{
	int		iHintNumber, iHintColor;
	int		iTotalCount;

	int		iHintCount		= ( lpcInput->nCount );
	int		iPrevColor		= -1;

	//	Zero line.
	if ( iHintCount == 0 ) {
		memset( &(lpOutput->nSquares[0]), SQUARE_CHECK, sizeof(Square) * nLength );
		return ( nLength );
	}

	iTotalCount		= 0;
	for ( int i = 0; i < iHintCount; i++ ) {
		iHintNumber		= ( lpcInput->nNumbers[i] );
		iHintColor		= ( lpcInput->nColors[i] );
		if ( iHintColor == 0 ) {
			iHintColor		= COLOR_BLACK;
		}
		if ( iHintColor == iPrevColor ) {
			iTotalCount ++;
		}
		iTotalCount		+= iHintNumber;
	}

	//	Over the line width.
	if ( iTotalCount > nLength ) {
		return ( -1 );
	}

	return ( 0 );
}

//============================================================================
//
//	Step the line.
//

//////////////////////////////////////////////////////////////////////////////
//	determineColors1
//	  This function determine squares where must be colored.

int determineColors1(int nLength,
					 LpcPicrossHintNumbers lpcHintNumbers,
					 LpcPicrossLineStatus lpcFixedPattern,
					 LptPicrossLineStatus lpResult,
					 int nFixedCount)
{
	int					i, x, iCount, iMake;
	int					iHeadLeft, iHeadRight, iTailLeft, iTailRight;
	int					iLeftSquare, iFixedSquare;
	int					iHintNumber, iHintColor, iNextColor;
	int					iHintCount	= ( lpcHintNumbers->nCount );
	PicrossLineStatus	headPattern, tailPattern;
	PatternGenerator	headGenerator( nLength, lpcHintNumbers );
	PatternGenerator	tailGenerator( nLength, lpcHintNumbers );

	//	Set fixed squares pattern.
	memcpy( lpResult, lpcFixedPattern, sizeof(PicrossLineStatus) );
	headGenerator.setupFixedPattern( lpcFixedPattern );
	tailGenerator.setupFixedPattern( lpcFixedPattern );
	iCount		= nFixedCount;

	//	Make pattern that put all blocks as left/right as possible.
	iMake	= headGenerator.makeFirstPattern( &headPattern );
	_ASSERTE( iMake > 0 );
	if ( iMake <= 0 ) {
		return ( iMake );
	}

	iMake	= tailGenerator.makeLastPattern( &tailPattern );
	_ASSERTE( iMake > 0 );
	if ( iMake <= 0 ) {
		return ( iMake );
	}

	for ( i = 0; i < iHintCount; i++ ) {
		//	Check range the i-th block can be stored.
		iHintNumber	= ( lpcHintNumbers->nNumbers[i] );
		iHeadLeft	= headGenerator.getBlockLeftPos( i );
		iTailLeft	= tailGenerator.getBlockLeftPos( i );
		iHeadRight	= iHeadLeft + iHintNumber;
		iTailRight	= iTailLeft + iHintNumber;

		for ( x = iTailLeft; x < iHeadRight; x++ ) {
			iFixedSquare	= ( lpcFixedPattern->nSquares[x] );
			iLeftSquare		= ( headPattern.nSquares[x] );

			_ASSERTE( IS_BLANK_SQUARE(iFixedSquare) || (iFixedSquare == iLeftSquare) );
			if ( !IS_BLANK_SQUARE(iFixedSquare) ) { continue; }
			if ( iLeftSquare & SQUARE_CHECK ) { break; }
			_ASSERTE( (tailPattern.nSquares[x]) == iLeftSquare );

			lpResult->nSquares[x]	= iLeftSquare;
			iCount	++;
		}

		//	Check next (left/right) square.
		if ( iHeadLeft == iTailLeft ) {
			iHintColor		= ( lpcHintNumbers->nColors[i] );
			//	Left side.
			if ( i > 0 ) {
				iNextColor		= ( lpcHintNumbers->nColors[i-1] );
				if ( iHintColor == iNextColor ) {
					x		= iHeadLeft - 1;
					iFixedSquare	= ( lpcFixedPattern->nSquares[x] );
					_ASSERTE( IS_BLANK_SQUARE(iFixedSquare) || (iFixedSquare == SQUARE_CHECK) );

					if ( IS_BLANK_SQUARE(iFixedSquare) ) {
						lpResult->nSquares[x]	= SQUARE_CHECK;
						iCount	++;
					}
				}
			}
			//	Right side.
			if ( i < iHintCount - 1 ) {
				iNextColor		= ( lpcHintNumbers->nColors[i+1] );
				if ( iHintColor == iNextColor ) {
					x		= iHeadLeft + iHintNumber;
					iFixedSquare	= ( lpcFixedPattern->nSquares[x] );
					_ASSERTE( IS_BLANK_SQUARE(iFixedSquare) || (iFixedSquare == SQUARE_CHECK) );

					if ( IS_BLANK_SQUARE(iFixedSquare) ) {
						lpResult->nSquares[x]	= SQUARE_CHECK;
						iCount	++;
					}
				}
			}
		}

		//	Check left/right white squares.
		if ( i == 0 ) {
			for ( x = 0; x < iHeadLeft; x++ ) {
				iFixedSquare	= ( lpcFixedPattern->nSquares[x] );
				_ASSERTE( IS_BLANK_SQUARE(iFixedSquare) || (iFixedSquare == SQUARE_CHECK) );
				if ( !IS_BLANK_SQUARE(iFixedSquare) ) { continue; }

				lpResult->nSquares[x]	= SQUARE_CHECK;
				iCount ++;
			}
		}
		if ( i == iHintCount - 1 ) {
			for ( x = iTailRight; x < nLength; x++ ) {
				iFixedSquare	= ( lpcFixedPattern->nSquares[x] );
				_ASSERTE( IS_BLANK_SQUARE(iFixedSquare) || (iFixedSquare == SQUARE_CHECK) );
				if ( !IS_BLANK_SQUARE(iFixedSquare) ) { continue; }

				lpResult->nSquares[x]	= SQUARE_CHECK;
				iCount ++;
			}
		}
	}

	return ( iCount );
}

//////////////////////////////////////////////////////////////////////////////
//	stepLineLv1
//	  Calculate the specified line (col/row).
//	Algorithm level 1.

int WINAPI	stepLineLv1(int nLength,
						LpcPicrossHintNumbers lpcInput,
						LpcPicrossLineStatus lpcInitPattern,
						LptPicrossLineStatus lpResult)
{
	int						iSpecial, iResult;
	int						iProgress;
	int						iFixedCount;
	PicrossLineStatus		savedInit, fixedPattern;

	g_nLevel		= 1;

	//	Save initial (fixed) pattern.
	memcpy( &fixedPattern, lpcInitPattern, sizeof(PicrossLineStatus) );
	memcpy( &savedInit, lpcInitPattern, sizeof(PicrossLineStatus) );
	iFixedCount		= takeIntersection( nLength, &savedInit, &fixedPattern );
	iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_START, &fixedPattern, lpResult, iFixedCount, nLength );

	//	Initialize pattern generator.
	PatternGenerator		patternGenerator( nLength, lpcInput );
	patternGenerator.setupFixedPattern( &fixedPattern );

	//	Check special patterns.
	iSpecial		= checkSpecialPatterns( nLength, lpcInput, &fixedPattern, lpResult );
	if ( iSpecial != 0 ) {
		return ( iSpecial );
	}

	//	Determine colored squares.
	iResult		= determineColors1( nLength, lpcInput, &fixedPattern, lpResult, iFixedCount );
	iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_END, &fixedPattern, lpResult, iFixedCount, iResult );

	//	Return the number of determined squares.
	return ( iResult );
}

//////////////////////////////////////////////////////////////////////////////
//	stepLineLv1
//	  Calculate the specified line (col/row).
//	Algorithm level 2.

int WINAPI	stepLineLv2(int nLength,
						LpcPicrossHintNumbers lpcInput,
						LpcPicrossLineStatus lpcInitPattern,
						LptPicrossLineStatus lpResult)
{
	int						iMake, iIntersection, iFirst, iSide;
	int						iTestResult, iProgress, iLevel1;
	int						iHeadCount, iFixedCount;
	PicrossLineStatus		currentPattern, savedInit, fixedPattern;

	g_nLevel		= 2;

	//	Save initial (fixed) pattern.
	memcpy( &fixedPattern, lpcInitPattern, sizeof(PicrossLineStatus) );
	memcpy( &savedInit, &fixedPattern, sizeof(PicrossLineStatus) );
	iFixedCount	= takeIntersection( nLength, &savedInit, &fixedPattern );

	//	Call level 1 algorithm.
	iLevel1		= stepLineLv1( nLength, lpcInput, lpcInitPattern, lpResult );
	memcpy( &fixedPattern, lpResult, sizeof(PicrossLineStatus) );
	iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_START,
		&fixedPattern, lpResult, iFixedCount, iLevel1 );

	//	Initialize pattern generator.
	PatternGenerator		patternGenerator( nLength, lpcInput );
	patternGenerator.setupFixedPattern( &fixedPattern );

	iSide		= checkSidePatterns( nLength, lpcInput, &fixedPattern, lpResult );
	if ( iSide < 0 ) {
		return ( iSide );
	}
	memcpy( &fixedPattern, lpResult, sizeof(PicrossLineStatus) );

	//	Save initial (fixed) pattern.
	memcpy( &savedInit, &fixedPattern, sizeof(PicrossLineStatus) );
	iFirst		= takeIntersection( nLength, &savedInit, &fixedPattern );
	iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_PROCEED, &fixedPattern, lpResult, iSide, iFirst );

	_ASSERTE( iFirst == iSide );

	//	Seive 1.
	iIntersection	= seive1( nLength, lpcInput, &fixedPattern, lpResult, iFirst );
	_ASSERTE( iIntersection >= iFirst );
	if ( iIntersection <= iFirst ) {
		iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_END,
			&fixedPattern, lpResult, iFixedCount, iIntersection );
		return ( iIntersection );
	}

	//	Check some last patterns.
	iTestResult		= iFirst;
	iMake		= patternGenerator.makeLastPattern( &currentPattern );
	iHeadCount	= 0;
	if ( iMake <= 0 ) { return ( -1 ); }
	iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );

	for ( iHeadCount = 0; iHeadCount < nLength; iHeadCount++ ) {
		iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_PROCEED,
			&currentPattern, lpResult, iIntersection, iTestResult );
		if ( iProgress == 0 )	{ return ( -3 ); }

		iMake	= patternGenerator.makePrevPattern( &currentPattern );
		if ( iMake == 0 )		{ break; }
		if ( iMake < 0 )		{ return ( iMake ); }

		iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );
		if ( iIntersection <= iTestResult ) { break; }
	}

	//	Seive 2. and update fixed pattern.
	if ( iIntersection <= iTestResult ) {
		iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_END,
			&fixedPattern, lpResult, iFixedCount, iIntersection );
		return ( iIntersection );
	}
	iTestResult	= seive2( nLength, lpcInput, &fixedPattern, lpResult, iIntersection );
	memcpy( &fixedPattern, lpResult, sizeof(PicrossLineStatus) );
	patternGenerator.setupFixedPattern( &fixedPattern );

	//	Make the first legal line pattern.
	iMake		= patternGenerator.makeFirstPattern( &currentPattern );
	iHeadCount	= 0;
	if ( iMake <= 0 ) { return ( -1 ); }
	iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );
	iProgress		= (*g_lpfnCallbackProgress)
		( g_nLevel, PROGRESS_PROCEED, &currentPattern, lpResult, 0, iIntersection );

	//	Make the other patterns, and take intersection for each.
	for ( iHeadCount = 0; iHeadCount < 0x00010000; iHeadCount++ ) {
		iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_PROCEED,
			&currentPattern, lpResult, iIntersection, iTestResult );
		if ( iProgress == 0 ) {
			return ( -3 );
		}

		iMake	= patternGenerator.makeNextPattern( &currentPattern );
		if ( iMake == 0 ) { break; }
		if ( iMake < 0 ) { return ( iMake ); }
		iHeadCount	++;

		iIntersection	= takeIntersection( nLength, lpResult, &currentPattern );
		if ( iIntersection <= iTestResult ) {
			break;
		}
	}

	iProgress	= (*g_lpfnCallbackProgress)( g_nLevel, PROGRESS_END,
		&fixedPattern, lpResult, iFixedCount, iIntersection );
	if ( iHeadCount >= 0x00010000 ) {
		memcpy( lpResult, &fixedPattern, sizeof(PicrossLineStatus) );
		return ( iFixedCount );
	}

	return ( iIntersection );
}
