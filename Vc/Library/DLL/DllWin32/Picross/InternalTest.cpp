//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		InternalTest.cpp													//
//		Do internal test.													//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"

//////////////////////////////////////////////////////////////////////////////
//
#if 0
int		executeInternalTest(int nLength,
							LPC_PICROSS_HINT_NUMBERS lpcInput,
							LPC_PICROSS_LINE_STATUS lpcInitPattern,
							LP_PICROSS_LINE_STATUS lpResult)
{
	int						x, iResult, nTestSquare;
	int						nFixedSquare, nFixedColor;
	int						iTestResult = 0;
	PICROSS_LINE_STATUS		lineTester, dummyLine;

	memcpy( &lineTester, lpcInitPattern, sizeof(PICROSS_LINE_STATUS) );

	//	Find determined square that is NOT included initial pattern.
	for ( x = 0; x < nLength; x++ ) {
		nFixedSquare		= ( lpcInitPattern->nSquares[x] );
		nFixedColor			= ( nFixedSquare & SQUARE_COLOR );

		nTestSquare			= ( lpResult->nSquares[x] );
		if ( (! IS_BLANK_SQUARE(nTestSquare)) && (nFixedSquare != nTestSquare) ) {
			if ( nTestSquare & SQUARE_CHECK ) {
#if 0
				//	Make test pattern.
				lineTester.nSquares[x]	= COLOR_BLACK;

				//	re-call stepTheLine, and check return value.
				iResult		= stepTheLine( nLength, lpcInput, &lineTester, &dummyLine, 2 );
				if ( iResult < 0 ) {
					lineTester.nSquares[x]		= SQUARE_CHECK;
				} else {
					lineTester.nSquares[x]		= SQUARE_BLANK;
					lpResult->nSquares[x]		= SQUARE_BLANK;
				}
				break;
#endif
			} else if ( (nTestSquare & SQUARE_COLOR) != 0 ) {
				//	Make test pattern.
				lineTester.nSquares[x]	= SQUARE_CHECK;

				//	re-call stepTheLine, and check return value.
				iResult		= makeFirstPattern( nLength, lpcInput, &lineTester, &dummyLine );
				if ( iResult < 0 ) {
					lineTester.nSquares[x]		= ( nTestSquare & SQUARE_COLOR );
					iTestResult ++;
				} else {
					lineTester.nSquares[x]		= SQUARE_BLANK;
					lpResult->nSquares[x]		= SQUARE_BLANK;
				}
				(*g_lpfnCallbackProgress)( &lineTester, lpResult, -1, -1 );

			}
		}
	}

	return ( iTestResult );
}
#endif
