//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		PreCompile.h														//
//		Pre-Compiled Header (PCH) File.										//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined( PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PRE_COMPILE_H )
#	 define   PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PRE_COMPILE_H

//============================================================================
//
//	Standard, System Include Files.
//

#define STRICT
#include <windows.h>

#include <crtdbg.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

//============================================================================
//
//	Picross and algorithm interface.
//
#include "Picross.h"
#include "PicrossDebug.h"

#endif
