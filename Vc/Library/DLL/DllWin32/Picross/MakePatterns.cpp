//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		MakePatterns.cpp													//
//		Generate legal patterns.											//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#include "PreCompile.h"

//////////////////////////////////////////////////////////////////////////////
//
//	Check if the specified pattern satisfy the condition
//	with the squares which specified by the fixed pattern.
//

//////////////////////////////////////////////////////////////////////////////

int		shiftLeftBlock(int nLength,
					   LPC_PICROSS_HINT_NUMBERS lpcInput,
					   LPC_PICROSS_LINE_STATUS lpcFixed,
					   LP_PICROSS_LINE_STATUS lpLine,
					   int iMoveBlock)
{
	int		i, x, iResult;
	int		iHintNumber;
	int		iCurColor, iPrevColor;

	for ( i = 0, x = 0; i <= iMoveBlock; i++ ) {
		iHintNumber		= ( lpcInput->nNumbers[i] );
		iCurColor		= ( lpcInput->nColors[i] );

		if ( iCurColor == 0 ) { iCurColor = COLOR_BLACK; }	//	Default is black.
		if ( iCurColor == iPrevColor ) { x++; }
		if ( lpLine->nSquares[x] == iCurColor ) {
			x			+= iHintNumber;
			iPrevColor	= iCurColor;
			iResult		= 0;
		} else {
			iResult		= 1;
		}
	}
	if ( iResult == 1 ) {
	}
	return ( 1 );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Make next pattern.
//	For this object, shift the most left/right color-sequences that moveable,
//	and put rest sequences as well as right/left.
//

//////////////////////////////////////////////////////////////////////////////
//
//	Make first/last legal pattern.
//

int		makeFirstPattern(int nLength,
						 LPC_PICROSS_HINT_NUMBERS lpcInput,
						 LPC_PICROSS_LINE_STATUS lpcFixed,
						 LP_PICROSS_LINE_STATUS lpOutput)
{
	int		x;
	int		iCheck, iResult;
	int		nHintCount;

	//	Generate the leftest pattern.
	memset( lpOutput, 0, sizeof(PICROSS_LINE_STATUS) );
	nHintCount		= ( lpcInput->nCount );
	x				= putBlocksOnLeft( nLength, lpcInput, 0, nHintCount - 1,
									   lpcFixed, lpOutput, 0 );
	memset( &(lpOutput->nSquares[x]), SQUARE_CHECK, sizeof(Square) * (nLength - x) );

	//	Test if pattern matched to fixed pattern.
	iCheck		= isPatternMatchedFixed( nLength, lpcFixed, lpOutput );
	if ( iCheck > 0 ) { return ( iCheck ); }

	//	Shift and test until test is succeeded.
	while ( iCheck <= 0 ) {
		//	Create next pattern.
		iResult		= shiftRightSequence( nLength, lpcInput, lpcFixed, lpOutput );
		if ( iResult < 0 ) {
			//	There are no legal patterns!!
			return ( iResult );
		}

		//	Check if this pattern satisfy fixed-pattern conditions.
		iCheck		= isPatternMatchedFixed( nLength, lpcFixed, lpOutput );
	}
	return ( iCheck );
}

int		makeLastPattern(int nLength,
						LPC_PICROSS_HINT_NUMBERS lpcInput,
						LPC_PICROSS_LINE_STATUS lpcFixed,
						LP_PICROSS_LINE_STATUS lpOutput)
{
	int		x;
	int		iCheck, iResult;
	int		nHintCount;

	//	Generate the leftest pattern.
	memset( lpOutput, 0, sizeof(PICROSS_LINE_STATUS) );
	nHintCount		= ( lpcInput->nCount );
	x				= putBlocksOnRight( nLength, lpcInput, 0, nHintCount - 1,
									    lpcFixed, lpOutput, nLength - 1 );
	memset( lpOutput->nSquares, SQUARE_CHECK, sizeof(Square) * x );

	//	Test if pattern matched to fixed pattern.
	iCheck		= isPatternMatchedFixed( nLength, lpcFixed, lpOutput );
	if ( iCheck > 0 ) { return ( iCheck ); }

	//	Shift and test until test is succeeded.
	while ( iCheck <= 0 ) {
		//	Create next pattern.
		iResult		= shiftLeftSequence( nLength, lpcInput, lpcFixed, lpOutput );
		if ( iResult < 0 ) {
			//	There are no legal patterns!!
			return ( iResult );
		}

		//	Check if this pattern satisfy fixed-pattern conditions.
		iCheck		= isPatternMatchedFixed( nLength, lpcFixed, lpOutput );
	}
	return ( iCheck );
}

//////////////////////////////////////////////////////////////////////////////
//
//	Make next/prev legal pattern, and overwrite 'lpLine'.
//

int		makeNextLegalPattern(int nLength,
							 LPC_PICROSS_HINT_NUMBERS lpcInput,
							 LPC_PICROSS_LINE_STATUS lpcFixed,
							 LP_PICROSS_LINE_STATUS lpLine)
{
	int		iCheck, iResult;

	iCheck	= 0;
	while ( iCheck <= 0 ) {
		//	Create next pattern.
		iResult		= shiftRightSequence( nLength, lpcInput, lpcFixed, lpLine );
		if ( iResult < 0 ) {
			return ( 0 );		//	There are no more legal patterns
		}

		//	Check if this pattern satisfy fixed-pattern conditions.
		iCheck		= isPatternMatchedFixed( nLength, lpcFixed, lpLine );
	}
	return ( 1 );
}

int		makePrevLegalPattern(int nLength,
							 LPC_PICROSS_HINT_NUMBERS lpcInput,
							 LPC_PICROSS_LINE_STATUS lpcFixed,
							 LP_PICROSS_LINE_STATUS lpLine)
{
	int		iCheck, iResult;

	iCheck	= 0;
	while ( iCheck <= 0 ) {
		//	Create next pattern.
		iResult		= shiftLeftSequence( nLength, lpcInput, lpcFixed, lpLine );
		if ( iResult < 0 ) {
			return ( 0 );		//	There are no more legal patterns
		}

		//	Check if this pattern satisfy fixed-pattern conditions.
		iCheck		= isPatternMatchedFixed( nLength, lpcFixed, lpLine );
	}
	return ( 1 );
}
