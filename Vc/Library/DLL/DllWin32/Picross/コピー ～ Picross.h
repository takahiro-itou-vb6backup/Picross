//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		Picross.h															//
//		Interface of Picross and AAS Algorithm.								//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined (PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PICROSS_H)
#define PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PICROSS_H

//============================================================================
//
//	Field status.
//

#define SQUARE_EMPTY	0		/* Empty (Not played) */
#define SQUARE_BLANK	0x80	/* Unknown (Not played, Equal to SQUARE_EMPTY) */
#define SQUARE_ASSUM	0x40	/* Assumption */
#define SQUARE_CHECK	0x20	/* Checked (Player judged white square) */
#define SQUARE_COLOR	0x0F	/* Color mask. (1 to 15) */

#define IS_BLANK_SQUARE(s)		( ((s) == SQUARE_EMPTY) || ((s) & SQUARE_BLANK) )
//#define IS_CHECKED_SQUARE(s)	( (s) & SQUARE_CHECK )

#define COLOR_BLACK		1

//============================================================================
//
//	Structures for single line.
//
#define MAX_HINTS_PER_LINE		64
#define MAX_SQUARES_PER_LINE	256

typedef unsigned char		Square;

//	Hint numbers.
typedef struct {
	int		nCount;
	int		nNumbers[MAX_HINTS_PER_LINE];
	int		nColors[MAX_HINTS_PER_LINE];
} PICROSS_HINT_NUMBERS;
typedef const PICROSS_HINT_NUMBERS *	LPC_PICROSS_HINT_NUMBERS;
typedef PICROSS_HINT_NUMBERS *			LP_PICROSS_HINT_NUMBERS;

//	Line status.
typedef struct {
	Square	nSquares[MAX_SQUARES_PER_LINE];
} PICROSS_LINE_STATUS;
typedef const PICROSS_LINE_STATUS *		LPC_PICROSS_LINE_STATUS;
typedef PICROSS_LINE_STATUS *			LP_PICROSS_LINE_STATUS;

//============================================================================
//
//	Prototype declarations.
//

typedef int (WINAPI * LPFN_CALLBACK_SHOWPROGRESS)(LP_PICROSS_LINE_STATUS,
												  LP_PICROSS_LINE_STATUS,
												  int, int);

extern	LPFN_CALLBACK_SHOWPROGRESS g_lpfnCallbackProgress;

extern	Square		gnEmptySquares[MAX_SQUARES_PER_LINE];
extern	Square		gnCheckSquares[MAX_SQUARES_PER_LINE];
extern	Square		gnBlackSquares[MAX_SQUARES_PER_LINE];

int		isPatternMatchedFixed(int nLength,
							  LPC_PICROSS_LINE_STATUS lpcFixed,
							  LPC_PICROSS_LINE_STATUS lpcPattern);

int		shiftLeftSequence(int nLength,
						  LPC_PICROSS_HINT_NUMBERS lpcInput,
						  LPC_PICROSS_LINE_STATUS lpcFixed,
						  LP_PICROSS_LINE_STATUS lpLine);
int		shiftRightSequence(int nLength,
						   LPC_PICROSS_HINT_NUMBERS lpcInput,
						   LPC_PICROSS_LINE_STATUS lpcFixed,
						   LP_PICROSS_LINE_STATUS lpLine);

int		makeFirstPattern(int nLength,
						 LPC_PICROSS_HINT_NUMBERS lpcInput,
						 LPC_PICROSS_LINE_STATUS lpcFixed,
						 LP_PICROSS_LINE_STATUS lpOutput);
int		makeLastPattern(int nLength,
						LPC_PICROSS_HINT_NUMBERS lpcInput,
						LPC_PICROSS_LINE_STATUS lpcFixed,
						LP_PICROSS_LINE_STATUS lpOutput);

int		makeNextLegalPattern(int nLength,
							 LPC_PICROSS_HINT_NUMBERS lpcInput,
							 LPC_PICROSS_LINE_STATUS lpcFixed,
							 LP_PICROSS_LINE_STATUS lpLine);
int		makePrevLegalPattern(int nLength,
							 LPC_PICROSS_HINT_NUMBERS lpcInput,
							 LPC_PICROSS_LINE_STATUS lpcFixed,
							 LP_PICROSS_LINE_STATUS lpLine);

int WINAPI	stepTheLine(int nLength,
						LPC_PICROSS_HINT_NUMBERS lpcInput,
						LPC_PICROSS_LINE_STATUS lpcInitPattern,
						LP_PICROSS_LINE_STATUS lpResult,
						int nOption);

#endif
