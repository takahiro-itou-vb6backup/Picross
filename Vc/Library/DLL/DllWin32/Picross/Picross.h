//////////////////////////////////////////////////////////////////////////////
//																			//
//		- Picross -			Picross Automatic Answer System (AAS).			//
//		Picross.h															//
//		Interface of Picross and AAS Algorithm.								//
//																			//
//		Copyright (c), 2006, Itou Takahiro, All rights reserved.			//
//////////////////////////////////////////////////////////////////////////////
#if !defined( PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PICROSS_H )
#	 define   PICROSS_AUTO_ANSWER_SYSTEM__INCLUDED__PICROSS_H

//============================================================================
//
//	Field status.
//

#define SQUARE_EMPTY	0		/* Empty (Not played) */
#define SQUARE_BLANK	0x80	/* Unknown (Not played, Equal to SQUARE_EMPTY) */
#define SQUARE_ASSUM	0x40	/* Assumption */
#define SQUARE_CHECK	0x20	/* Checked (Player judged white square) */
#define SQUARE_COLOR	0x0F	/* Color mask. (1 to 15) */

#define IS_BLANK_SQUARE(s)		( ((s) == SQUARE_EMPTY) || ((s) & SQUARE_BLANK) )
//#define IS_CHECKED_SQUARE(s)	( (s) & SQUARE_CHECK )

#define COLOR_BLACK		1

//============================================================================
//
//	Structures for single line.
//
#define MAX_HINTS_PER_LINE		64
#define MAX_SQUARES_PER_LINE	256

typedef unsigned char		Square;

//	Hint numbers.
typedef struct {
	int		nCount;
	int		nNumbers[MAX_HINTS_PER_LINE];
	int		nColors[MAX_HINTS_PER_LINE];
} PicrossHintNumbers;

typedef PicrossHintNumbers *		LptPicrossHintNumbers;
typedef const PicrossHintNumbers *	LpcPicrossHintNumbers;

//	Line status.
typedef struct {
	Square	nSquares[MAX_SQUARES_PER_LINE];			//	Square cells pattern
	int		nBlockIndex[MAX_SQUARES_PER_LINE];		//	which block the color belongs to
} PicrossLineStatus;

typedef PicrossLineStatus *			LptPicrossLineStatus;
typedef const PicrossLineStatus *	LpcPicrossLineStatus;

//============================================================================
//
//	Macros in order to access data.
//

//============================================================================
//
//	Prototype declarations.
//

typedef int (WINAPI * LPFN_CALLBACK_SHOWPROGRESS)(int, int,
												  LptPicrossLineStatus,
												  LptPicrossLineStatus,
												  int, int);

extern	LPFN_CALLBACK_SHOWPROGRESS g_lpfnCallbackProgress;
#define	PROGRESS_START		0
#define	PROGRESS_CREATE		1
#define PROGRESS_PROCEED	2
#define	PROGRESS_END		3

extern	Square		gnEmptySquares[MAX_SQUARES_PER_LINE];
extern	Square		gnCheckSquares[MAX_SQUARES_PER_LINE];
extern	Square		gnBlackSquares[MAX_SQUARES_PER_LINE];

int WINAPI	stepLineLv1(int nLength,
						LpcPicrossHintNumbers lpcInput,
						LpcPicrossLineStatus lpcInitPattern,
						LptPicrossLineStatus lpResult,
						int nOption);

extern	int		g_nLevel;

#endif
